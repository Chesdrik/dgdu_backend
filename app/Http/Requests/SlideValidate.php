<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SlideValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'title' => 'required',
             // 'url' => 'required',
             'active' => 'required',
             'order' => 'required',
             // 'img' => 'required',
        ];
    }

    public function messages()
{
    return [
        'title.required' => 'Es necesario ingresar un título',
        'active.required' => 'Es necesario definir si está activo o no el slider.',
        'order.required' => 'Es necesario definir el orden.',
    ];
}
}
