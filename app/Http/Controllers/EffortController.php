<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Effort;

class EffortController extends Controller
{
    public function kml_download($effort_id)
    {
        // $user = User::find($id);
        $effort = Effort::find($effort_id);

        $kml = array();
        $kml[] = '<?xml version="1.0" encoding="UTF-8"?>';
        $kml[] = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">';
        $kml[] = ' <Document>';
        $kml[] = '<name>ruta_' . $effort->id . '.kml</name>';
        $kml[] = ' <Schema name="ruta_' . $effort->id . '" id="S_ruta_' . $effort->id . '_DD">';
        $kml[] = ' <SimpleField type="double" name="lat"><displayName>&lt;b&gt;lat&lt;/b&gt;</displayName>';
        $kml[] = '</SimpleField>';
        $kml[] = '<SimpleField type="double" name="_long"><displayName>&lt;b&gt; long&lt;/b&gt;</displayName>';
        $kml[] = '</SimpleField>';
        $kml[] = '</Schema>';
        $kml[] = '<StyleMap id="pointStyleMap">';
        $kml[] = ' <Pair>';
        $kml[] = '  <key>normal</key>';
        $kml[] = '<styleUrl>#normPointStyle</styleUrl>';
        $kml[] = '</Pair>';
        $kml[] = '<Pair>';
        $kml[] = '  <key>highlight</key>';
        $kml[] = ' <styleUrl>#hlightPointStyle</styleUrl>';
        $kml[] = '</Pair>';
        $kml[] = '</StyleMap>';
        $kml[] = '<Style id="normPointStyle">';
        $kml[] = ' <IconStyle>';
        $kml[] = ' <Icon>';
        $kml[] = ' <href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>';
        $kml[] = '</Icon>';
        $kml[] = '</IconStyle>';
        $kml[] = '<BalloonStyle>';
        $kml[] = '<text><![CDATA[<table border="0">
          <tr><td><b>lat</b></td><td>$[ruta_' . $effort->id . '/lat]</td></tr>
          <tr><td><b> long</b></td><td>$[ruta_' . $effort->id . '/_long]</td></tr>
        </table>
        ]]>';
        $kml[] = '</text>';
        $kml[] = '</BalloonStyle>';
        $kml[] = '</Style>';
        $kml[] = '<Style id="hlightPointStyle">';
        $kml[] = '<IconStyle>';
        $kml[] = '<Icon>';
        $kml[] = '<href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png</href>';
        $kml[] = '</Icon>';
        $kml[] = '</IconStyle>';
        $kml[] = '<BalloonStyle>';
        $kml[] = '<text><![CDATA[<table border="0">
          <tr><td><b>lat</b></td><td>$[ruta_' . $effort->id . '/lat]</td></tr>
          <tr><td><b> long</b></td><td>$[ruta_' . $effort->id . '/_long]</td></tr>
        </table>
        ]]>';
        $kml[] = '</text>';
        $kml[] = '</BalloonStyle>';
        $kml[] = '</Style>';
        $kml[] = '<Folder id="layer 0">';
        $kml[] = ' <name>ruta_' . $effort->id . '</name>';

        foreach (json_decode($effort->effort_gps_locations->first()->route) as $value) {
            $coordinates = explode(',', $value);
            $kml[] = ' <Placemark>';
            $kml[] = ' <styleUrl>#pointStyleMap</styleUrl>';
            $kml[] = ' <Style id="inline">';
            $kml[] = ' <IconStyle>';
            $kml[] = ' <color>ffededed</color>';
            $kml[] = ' <colorMode>normal</colorMode>';
            $kml[] = ' </IconStyle>';
            $kml[] = ' <LineStyle>';
            $kml[] = ' <color>ffededed</color>';
            $kml[] = ' <colorMode>normal</colorMode>';
            $kml[] = ' </LineStyle>';
            $kml[] = ' <PolyStyle>';
            $kml[] = ' <color>ffededed</color>';
            $kml[] = ' <colorMode>normal</colorMode>';
            $kml[] = ' </PolyStyle>';
            $kml[] = ' </Style>';
            $kml[] = ' <ExtendedData>';
            $kml[] = ' <SchemaData schemaUrl="#S_ruta_' . $effort->id . '_DD">';
            $kml[] = ' <SimpleData name="lat">' . $coordinates[0] . '</SimpleData>';
            $kml[] = ' <SimpleData name="_long">' . $coordinates[1] . '</SimpleData>';
            $kml[] = ' </SchemaData>';
            $kml[] = ' </ExtendedData>';
            $kml[] = ' <Point>';
            $kml[] = ' <coordinates>' . $coordinates[1] . ',' . $coordinates[0] . '</coordinates>';
            $kml[] = ' </Point>';
            $kml[] = ' </Placemark>';
        }

        // Fin del Archivo XML
        $kml[] = ' </Folder>';
        $kml[] = ' </Document>';
        $kml[] = ' </kml>';


        // Genero el archivo y lo pongo para descarga directa.
        $kmlOutput = join("\n", $kml);

        return \Response::make($kmlOutput, 200, [
            'Content-type' => 'application/vnd.google-earth.kml+xml',
            'Content-Disposition' => 'attachment; filename="ruta' . $effort->id . '.kml"'
        ]);
    }
}
