<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }


        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $this->clearLoginAttempts($request);


            if (Auth::user()->user_type_id == 1) {
                // Everything ok
                return redirect()->intended('/dashboard');
            } else {
                // Not admin
                return back()->with('error', 'Acceso sólo a usuarios administradores');
            }
        } else {
            $this->incrementLoginAttempts($request);

            // Incorrect user/password
            return back()->with('error', 'Usuario o contraseña incorrectos');
        }

        // return (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_type_id' => 1])) ? redirect()->intended('/dashboard') : back()->with('error', 'Acceso sólo a usuarios administradores');
    }
}
