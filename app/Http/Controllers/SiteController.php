<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Models\Encrypter;

class SiteController extends Controller
{
    public function home()
    {
        return view('auth.login');
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function encryption()
    {
        // Creating encrypter
        $encrypter = new Encrypter();
        $message = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ultrices orci vel dui convallis, at rhoncus est auctor.';
        $encrypter->setMessage($message);
        dump("Encrypted message: $encrypter->encrypted_message");
        dump("Encrypted message in base64: " . base64_encode($encrypter->encrypted_message));

        // Creating decrypter
        $decrypter = new Encrypter();
        $decrypted_message = $decrypter->decryptMessage($encrypter->encrypted_message);
        // $decrypted_message = $decrypter->decryptMessage("RNpJ/6SUyMHwBHE9F0vxM52V4j7q2gPYqKVYrStufJQF2HVS7wsONxVZXi37Qw25c1gHq8kbGAy1eEnlQFpPIdoZIeBG75eBLvjqZILsC/HLNASR8/xnIbBvCI5g0niyrP1DtQIP7GrabhzaraAuFmJ2y5H2G0LWYnyMRa5Nqqk=");
        // $decrypted_message = $decrypter->decryptMessage("Y09uZ3pBdlJiK2xrVnJ4SVNZU3NGZz09Uk5wSi82U1V5TUh3QkhFOUYwdnhNNTJWNGo3cTJnUFlxS1ZZclN0dWZKUUYySFZTN3dzT054VlpYaTM3UXcyNWMxZ0hxOGtiR0F5MWVFbmxRRnBQSWRvWkllQkc3NWVCTHZqcVpJTHNDL0hMTkFTUjgveG5JYkJ2Q0k1ZzBuaXlyUDFEdFFJUDdHcmFiaHphcmFBdUZtSjJ5NUgyRzBMV1lueU1SYTVOcXFrPQ==");


        // dump("iv_cipher_text_b54: $encrypter->iv_cipher_text_b54");
        dump("Decrypted: $decrypted_message");
        dd('end');
    }
}
