<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use App\Http\Requests\UsersValidate;
use App\Http\Requests\UserEditValidate;

class UserController extends Controller
{


    public function index($users = null)
    {
        // Filtering users and defining parameter
        $user_type = (is_null($users) || $users == 'administradores' ? 'administradores' : 'app');
        $user_types = UserType::where('id', '!=', 1)->orderBy('name')->pluck('name', 'id');
        $users = ($user_type == 'administradores' ? User::admins() : User::appUsers());
        $random_pswd = \Str::random(8);

        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Usuarios', 'url' => route('users.index'), 'active' => true],
        ];
        // $users = User::orderBy('name')->get();
        // $type = UserType::pluck('name', 'id');

        return view('users.list', compact('breadcrumb', 'users', 'user_type', 'user_types', 'random_pswd'));
    }

    public function appUsers()
    {
        return $this->index('appUsers');
    }



    // public function noadmin()
    // {
    //     $breadcrumb = [
    //         ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
    //         ['label' => 'Usuarios', 'url' => route('users.index'), 'active' => false],
    //         ['label' => 'Usuarios no administrador', 'url' => '#', 'active' => true],
    //     ];
    //     $users = User::where('user_type_id', '!=', 1)->get();

    //     return view('users.no_admin', compact('breadcrumb', 'users'));
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersValidate $request)
    {
        $request->request->add([
            'password' => bcrypt($request->password),
            // 'user_type_id' => 1,
        ]);
        $user = User::create($request->except('_token'));

        return back()->with('success', 'Usuario creado corractamente');;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditValidate $request, User $users)
    {

        $users->update($request->except('_token'));
        return back()->with('success', 'Usuario actualizada corractamente');;
    }

    public function efforts($id)
    {
        $user = User::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Usuarios', 'url' => route('users.index'), 'active' => false],
            ['label' => $user->name, 'url' => "#", 'active' => false],
            ['label' => 'Esfuerzo', 'url' => '#', 'active' => true],
        ];

        // $data = array();
        // foreach ($user->effort as $effort) {

        //     $data[$effort->id]['name'] = $effort->effort_type->name;
        //     foreach ($effort->stats as $stat) {
        //         //dd($stat->type);
        //         $data[$effort->id][$stat->type->name] = $stat->value;
        //     }
        // }
        return view('users.effort', compact('user', 'breadcrumb'));
    }
}
