<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Location;


class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = Content::all();
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Textos', 'url' => '#', 'active' => true],
        ];


        return view('text.list', compact('breadcrumb', 'contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = Content::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Textos', 'url' => '#', 'active' => true],
        ];


        return view('text.edit', compact('breadcrumb', 'content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $content = Content::find($id);
        $content->update(['key'=> $request->title, 'content'=>$request->content]);
        $lat;
        $long;
        if ($request->lat == 0 or $request->long == 0) {
            $lat = 0;
            $long = 0;
        }else{
            $lat = $request->lat;
            $long = $request->long;
        }
        
        $flight = Location::updateOrCreate(
            ['content_id' => $content->id],
            ['lat' => $lat, 'long' => $long]
        );

        return back()->with('success', 'Texto editado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Content  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = Content::find($id);
        $content->location()->delete();
        $content->delete();
        return redirect('/textos')
            ->with('success', 'Texto eliminado correctamente');
    }
}
