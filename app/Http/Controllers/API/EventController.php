<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Event;
use App\Models\Team;

class EventController extends Controller
{
    /**
     * Register user to event
     * @param Request $request Request object
     * @return array
     *
     */
    public function register_to_event(Request $request)
    {
        // Fetching user
        $user = User::find($request->user_id);

        // Verify if user is already registered to event
        if ($user->events()->wherePivot('event_id', $request->event_id)->count() > 0) {
            return [
                'error' => true,
                'error_message' => "Ya te encuentras inscrito a este evento"
            ];
        }

        // Define pivot info
        $pivot_event_user = [
            'category_id' => $request->category_id,
        ];

        // Checking if user is registering to a team
        if (!is_null($request->team_name)) {
            // Check if team exists
            $team_exists = Team::where([
                'event_id' => $request->event_id,
                'name' => $request->team_name
            ])->count();


            if ($team_exists == 0) {
                // New team, we just create it
                $team = Team::create([
                    'event_id' => $request->event_id,
                    'name' => $request->team_name,
                    'api_password' => Hash::make($request->team_password)
                ]);
            } else {
                // Fetching team
                $team = Team::where([
                    'event_id' => $request->event_id,
                    'name' => $request->team_name,
                ])->first();

                // Check if password is correct
                if (!Hash::check($request->team_password, $team->api_password)) {
                    return [
                        'error' => true,
                        'error_message' => "Contraseña incorrecta para el equipo {$team->name}.",
                    ];
                }
            }

            // Fetch event
            $event = Event::find($request->event_id);

            // Count current users in team
            $total_users_in_team = $team->users()->wherePivot('event_id', $request->event_id)->count();

            // Checking if we can add more users to the team
            if ($total_users_in_team >= $event->max_users) {
                return [
                    'error' => true,
                    'error_message' => "El equipo {$request->team_name} ya tiene el máximo de usuarios permitidos"
                ];
            }

            // Appending data to pivot array
            $pivot_event_user['team_id'] = $team->id;
        }

        // Attaching to event
        $user->events()->attach([
            $request->event_id => $pivot_event_user
        ]);

        // Incrementing current_users and creating folio
        $event->increment('current_users', 1);
        $user->events()->updateExistingPivot($event, ['folio' => str_pad($event->current_users, 6, 0, STR_PAD_LEFT)]);

        // R1eturning response
        return [
            'error' => false,
        ];
    }
}
