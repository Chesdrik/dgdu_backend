<?php

namespace App\Http\Controllers;

use App\Models\EffortType;
use Illuminate\Http\Request;

class EffortTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EffortType  $effortType
     * @return \Illuminate\Http\Response
     */
    public function show(EffortType $effortType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EffortType  $effortType
     * @return \Illuminate\Http\Response
     */
    public function edit(EffortType $effortType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EffortType  $effortType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EffortType $effortType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EffortType  $effortType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EffortType $effortType)
    {
        //
    }
}
