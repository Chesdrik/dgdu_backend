<?php

namespace App\Http\Controllers;

use App\Models\StatType;
use Illuminate\Http\Request;

class StatTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatType  $statType
     * @return \Illuminate\Http\Response
     */
    public function show(StatType $statType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatType  $statType
     * @return \Illuminate\Http\Response
     */
    public function edit(StatType $statType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatType  $statType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatType $statType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatType  $statType
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatType $statType)
    {
        //
    }
}
