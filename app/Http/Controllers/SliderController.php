<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Requests\SlideValidate;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sliders.list', [
            'sliders' => Slider::all()->sortBy('order'),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
                ['label' => 'Sliders', 'url' => route('events.index'), 'active' => true],
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SlideValidate $request)
    {
        $slider = Slider::create($request->except('_token', 'img'));
        $this->uploadImage($request, $slider);

        return redirect(route('sliders.index'))
            ->with('success', 'Slider creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        return view('sliders.show', [
            'slider' => $slider,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
                ['label' => 'Sliders', 'url' => route('sliders.index'), 'active' => true],
                ['label' => $slider->title, 'url' => route('sliders.show', $slider->id), 'active' => true],
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(SlideValidate $request, Slider $slider)
    {
        $slider->update($request->except('_token', 'img'));
        $this->uploadImage($request, $slider);

        return back()->with('success', 'Slider actualizado corractamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {

        if (\File::exists('storage/sliders/'.$slider->id)){
            \File::deleteDirectory('storage/sliders/'.$slider->id);
        }
        $slider->delete();
        return redirect(route('sliders.index'))
            ->with('success', 'Slider eliminado correctamente');
    }

    public function uploadImage(Request $request, Slider $slider){
        // Uploading image
        if ($request->hasFile('img') && $request->file('img')->isValid()){
            // $filename = $event->id.'.'.$request->img->getClientOriginalExtension();
            $filename = $slider->id.'.'.$request->img->getClientOriginalExtension();

            // Uploading file
            $request->file('img')->move('storage/sliders/'.$slider->id, $filename);
            $slider->update([
                'img' => $filename
            ]);
        }
    }
}
