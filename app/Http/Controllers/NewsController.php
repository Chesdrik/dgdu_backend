<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Requests\NewsValidate;
use App\Http\Requests\NewsUpdate;
use \Vedmant\FeedReader\Facades\FeedReader;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Noticias', 'url' => route('news.index'), 'active' => true],
        ];
        $news = News::orderBy('date')->get();

        return view('news.list', compact('breadcrumb', 'news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Noticias', 'url' => route('news.index'), 'active' => false],
            ['label' => 'Registrar', 'url' => '#', 'active' => true],
        ];

        return view('news.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsValidate $request)
    {

        $news = News::create($request->except('_token', 'img'));
        $this->uploadImage($request, $news);

        return  redirect()->route('news.index')->with('success', 'Noticia creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Noticias', 'url' => route('news.index'), 'active' => false],
            ['label' => 'Editar', 'url' => '#', 'active' => true],
        ];

        return view('news.edit', compact('breadcrumb', 'news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(NewsUpdate $request, News $news)
    {
        // Updating news
        $news->update($request->except('_token', 'img'));

        // Checking if there is an image
        if ($request->img != null) {
            $this->uploadImage($request, $news);
        }

        // Returning with success
        return back()->with('success', 'Noticia actualizada corractamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if (\File::exists('storage/' . $news->path())) {
            \File::deleteDirectory('storage/' . $news->path());
        }

        $news->delete();

        return redirect()->route('news.index')->with('success', 'Noticia eliminada');
    }


    public function uploadImage(Request $request, News $news)
    {
        // Uploading image
        if ($request->hasFile('img') && $request->file('img')->isValid()) {
            // $filename = $event->id.'.'.$request->img->getClientOriginalExtension();
            $filename = $news->id . '_news.png';

            // Uploading file
            $request->file('img')->move('storage/news/' . $news->id . '/', $filename);
            $news->update([
                'img' => $filename
            ]);
        }
    }

    public function rss_fetch()
    {
        $rss_feed = FeedReader::read('https://deporte.unam.mx/rss.php');
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Noticias', 'url' => route('news.index'), 'active' => false],
            ['label' => 'Registar con RSS', 'url' => '#', 'active' => true],
        ];

        return view('news.rss_feed', compact('breadcrumb', 'rss_feed'));
    }
}
