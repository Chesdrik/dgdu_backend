<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = video::orderBy('order')->get();
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Videos', 'url' => '#', 'active' => true],
        ];


        return view('videos.list', compact('breadcrumb', 'videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Videos', 'url' => route('video.index'), 'active' => false],
            ['label' => 'Crear', 'url' => '#', 'active' => true],
        ];


        return view('videos.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $video = Video::where('order', $request->order)->first();
        if (!is_null($video)) {
            $order = $video->order+1;
            $video->update(['order' => $order]);
        }
        Video::create($request->except('_token'));
        return redirect()->route('video.index')->with('success', 'Playlist creada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
      
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
            ['label' => 'Videos', 'url' => route('video.index'), 'active' => false],
            ['label' => 'Editar', 'url' => '#', 'active' => true],
        ];


        return view('videos.edit', compact('breadcrumb', 'video'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $change_order = Video::where('order', $request->order)->first();
        if (!is_null($change_order)) {
            if ($change_order->id != $video->id) {
                $order = $change_order->order+1;
                $change_order->update(['order' => $order]);
            }
        }
        $video->update($request->except('_token'));
        return back()->with('success', 'Playlist editada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        $video->delete();
        return back()->with('success', 'Playlist eliminada correctamente');
    }
}
