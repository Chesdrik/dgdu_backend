<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use App\Models\EffortGpsLocation;
use Illuminate\Http\Request;
use App\Models\EventType;
use App\Models\EffortType;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('events.list', [
            'past_events' => Event::past_events(),
            'upcoming_events' => Event::upcoming_events(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
                ['label' => 'Eventos', 'url' => route('events.index'), 'active' => true],
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create', [
            'event_types_array' => EventType::event_types_array(),
            'effort_types_array' => EffortType::effort_types_array(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::create($request->except('_token'));

        return redirect(route('events.index'))
            ->with('success', 'Evento creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($event_id)
    {
        $event = Event::find($event_id);

        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $event->update($request->except('_token'));

        return redirect(route('events.index'))
            ->with('success', 'Evento actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
