<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Http;

use App\Models\User;
use App\Models\Stat;
use App\Models\Effort;
use App\Models\EffortGpsLocation;
use App\Models\EffortUser;
use App\Models\StatType;
use App\Models\Event;
use App\Models\EventType;
use App\Models\Slider;
use App\Models\News;
use App\Models\Team;
use App\Models\Video;
use App\Models\Content;
use App\Models\Category;
use App\Models\SOAP\SOAP;

use App\Http\Resources\UserResource;

use Carbon\Carbon;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use App\Models\Encrypter;

class APIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function login(Request $request)
    {
        // Validating request
        $validation = Validator::make($request->all(), [
            'email' => 'required|exists:users|email',
            'password' => 'required|min:5'
        ], [
            'email.required' => 'Es necesario ingresar un email',
            'email.email' => 'El texto ingresado no es un email',
            'email.exists' => 'No encontramos el email en nuestros registros.',
            'password.required' => 'Es necesario ingresar un password',
            'password.min' => 'Tu password tiene que ser de al menos 5 caracteres'
        ]);

        $return = array();
        if ($validation->fails()) {
            $return['error'] = true;
            $return['message'] = implode(", ", $validation->messages()->all());
        } else {
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                $user = User::where('email', $request->email)->first();

                // This is where we would register tokens, IF I HAD ANY!
                /*  if(!empty($request->token)){
                    $token = Token::updateOrCreate(['token' => $request->token], ['user_id' => $user->id, 'type' => $request->type]);
                }*/

                $return['error'] = false;
                $return['message'] = "";
                $return['user'] = $user->APIprofile();
                // $return['user_favs'] = $user->favs_ids;
            } else {
                $return['error'] = true;
                $return['message'] = "Email y contraseña incorrectos";
            }
        }

        return $return;
    }

    public function listEvent()
    {
        // Fetching events
        $events = Event::where('active', 1)->get();

        // Checking if there is something to return
        if (!is_null($events->first())) {
            $data = array();

            // Process each event
            foreach ($events as $event) {
                // Append event data
                $data[] = [
                    'id' => $event->id,
                    'name' => $event->name,
                    'date' => $event->start ?? "2000-01-01 00:00:00",
                    'active' => $event->active,
                ];
            }

            // Return event data
            return [
                'error' => false,
                'events' => $data
            ];
        } else {
            // Return error
            return [
                'error' => true,
                'message' => 'No se pudo obtener la información requerida'
            ];
        }
    }

    public function eventUser(Request $request, $user_id)
    {
        try {
            // \Log::info($request->all());
            $user = User::find($user_id);

            $soap = new SOAP('login');
            $evento_result = $soap->leeEvento($user->CURP, $request->password);

            // \Log::info((array)$evento_result);
            $data = (array) $evento_result->LeeEventosResult;

            if ($data['NoEventos'] != 0) {
                foreach ($data['InfoEvento'] as $eventos) {
                    foreach ($eventos as $v) {
                        $events = Event::where('noid', $v->NOID)->get()->first();
                        $event_type = EventType::where('name', $v->Tipo)->firstOrFail();
                        // \Log::info($event_type);

                        $event = Event::updateOrCreate(
                            ['name' => $v->NomEvento, 'noid' => $v->NOID],
                            ['date' => $v->Inicia, 'event_type_id' => $event_type->id]
                        );

                        if (is_null($user->events_user($events->id))) {

                            $category = Category::where('name', $v->Categoria)->get()->first();
                            //$user->events_user($events->id)->syncWithoutDetaching(['folio'=> $eventos->NoCorredor],['category_id' => $category->id]);
                            $user->event()->syncWithoutDetaching([$events->id => ['folio' => $v->NoCorredor, 'category_id' => $category->id]]);
                        }
                    }
                }
            }
            //dd($data);

            //$user->events()

            if (is_null($user))
                return [
                    'error' => false,
                    'message' => 'No se encontró usuario con el ID: ' . $user_id
                ];

            $data = array();
            foreach (Event::all() as $event) {
                $data[] = [
                    'id' => $event->id,
                    'name' => $event->name,
                    'date' => $event->start ?? "2000-01-01 00:00:00",
                    'asociado' => in_array($event->id, $user->event_array()) ? true : false
                ];
            }

            return [
                'error' => false,
                'events' => $data
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo obtener la información requerida',
                'error' => $th,
            ];
        }
    }

    public function detailEvent($event_id)
    {
        $event = Event::find($event_id)->toArray();
        return [
            'error' => false,
            'event' => $event
        ];
    }

    public function eventDetail($user_id, $event_id)
    {
        try {
            $event = Event::find($event_id);
            $user = User::find($user_id);

            if (is_null($event) || is_null($user))
                return [
                    'error' => true,
                    'message' => 'No se encontraron datos con los IDs dados'
                ];

            $relation = $user->events()->where('event_id', $event_id)->first();
            $date = Carbon::parse($event->date)->format('Y-m-d');
            /* if ($event->date > Carbon::now()) {
                //$category = Category::find($relation->pivot->category_id);
                $data = [
                    'name' => $event->name,
                    'date' => $date,
                    'active' => $event->active,
                    //'range' => $category->name,
                    'distance' => '',
                    'route' => ''
                ];
            } else {*/
            if (!is_null($relation)) {
                $category = Category::find($relation->pivot->category_id);

                $data = [
                    'name' => $event->name,
                    'date' => $date,
                    'active' => $event->active,
                    'range' => $category->name,
                    'distance' => '',
                    'route' => '',
                    'time' => '',
                    'speed' => '',
                    'user_effort' => ''
                ];
            } else {
                $data = [
                    'name' => $event->name,
                    'date' => $date,
                    'active' => $event->active,
                    'range' => '',
                    'distance' => '',
                    'route' => '',
                    'time' => '',
                    'speed' => '',
                    'user_effort' => ''
                ];
            }
            //}

            return $event;
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo obtener detalle del evento'
            ];
        }
    }

    public function dashboard()
    {
        try {
            return [
                'error' => false,
                'sliders' => Slider::slidersForApp(),
                'news' => News::newsForApp()
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'Error al obtener información'
            ];
        }
    }


    public function register_effort(Request $request)
    {
        // Log request
        // \Log::debug($request->all());

        $return = [
            'error' => false
        ];

        $new_user = null;

        if (isset($request->device_id)) {
            $user = User::where('device_id', $request->device_id)->first();
            if (is_null($user)) {
                $new_user = User::create([
                    'user_type_id' => 2,
                    'name' => 'Usuario_' . $request->device_id,
                    'device_id' => $request->device_id
                ]);

                $return['new_user'] = true;
                $return['user_id'] = $new_user->id;
            }
        } else {

            $user = User::find($request->user_id);
        }



        // Registering effort
        $effort = Effort::create([
            'user_id' => ($new_user) ? $new_user->id : $user->id,
            'effort_type_id' => $request->effort_type_id,
            'start' => $request->effort_start,
            'end' => $request->effort_end,
        ]);

        $beginning = Carbon::parse($effort->start);

        $ending = Carbon::parse($effort->end);

        $min = $beginning->diffInMinutes($ending);
        $diff = Carbon::parse('00:00:00');
        $diff = $diff->addMinute($min)->format('H:i:s');
        //dd($diff);

        $effort->stats()->updateOrCreate([
            'stat_type_id' => 1,
        ], [
            'value' => $diff
        ]);

        // Registering time
        /* if (isset($request->time)) {
            $effort->stats()->updateOrCreate([
                'stat_type_id' => 1,
            ], [
                'value' => $request->time
            ]);
        }*/

        // Registering distance
        if (isset($request->distance)) {
            $effort->stats()->updateOrCreate([
                'stat_type_id' => 2,
            ], [
                'value' => $request->distance
            ]);
        }

        // Registering location data
        if (isset($request->gps_locations)) {
            $locations = EffortGpsLocation::create([
                'effort_id' => $effort->id,
                'route' => $request->gps_locations,
            ]);
        }
        // Registering location data
        if (isset($request->step)) {
            $effort->stats()->updateOrCreate([
                'stat_type_id' => 6
            ], [
                'value' => $request->step
            ]);
        }
        // Registering location data
        if (isset($request->speed)) {
            $effort->stats()->updateOrCreate([
                'stat_type_id' => 3,
            ], [
                'value' => $request->speed
            ]);
        }
        // Registering location data
        if (isset($request->heart_rate)) {
            $effort->stats()->updateOrCreate([
                'stat_type_id' => 4,
            ], [
                'value' => $request->heart_rate
            ]);
        }
        // Registering location data
        if (isset($request->calories)) {
            $effort->stats()->updateOrCreate([
                'stat_type_id' => 5,
            ], [
                'value' => $request->calories
            ]);
        }

        if (isset($request->event_id)) {
            $user = ($new_user) ? User::find($new_user->id) : User::find($request->user_id);
            $user->events()->updateOrCreate([
                'event_id' => $request->event_id,
            ], [
                'effort_id' => $effort->id,
            ]);
        }

        return $return;
    }

    public function update_user_data(Request $request)
    {
        // Check if data is complete
        if (is_null($request->email) || is_null($request->password)) {
            return [
                'error' => true,
                'error_message' => 'Es necesario proporcionar usuario y password para actualizar la información'
            ];
        }

        // Check user auth info

        // Update data from webservice
        $this->updateUserData($request->email, $request->password);

        // If user has email, we update data from webservice
        $user = User::find($request->id);
        $weight = $user->weight;
        $height = $user->height;
    }

    public function effort_detail(Request $request)
    {
        // \Log::info($request->all());

        // Fetching user and parsing dates
        try {
            // Checking if there is any data at all
            if (is_null($request->user_id) && is_null($request->device_id)) {
                return [
                    'error' => true,
                    'message' => 'Usuario no encontrado'
                ];
            }

            // Fetching user from id or device id
            if (!is_null($request->user_id)) {
                $user = User::findOrFail($request->user_id);
                $age = Carbon::parse($user->birthdate)->age;
                $weight = $user->weight;
                $height = $user->height;
            } elseif (!is_null($request->device_id)) {
                $user = User::where('device_id', $request->device_id)->first();
                $age = '0';
                $weight = '0';
                $height = '0';
            }

            // Could not find user
            if (is_null($user)) {
                return [
                    'error' => true,
                    'message' => 'Usuario no encontrado'
                ];
            }


            // Defining timestamp
            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d 00:00:00');
            $end = $now->endOfWeek()->format('Y-m-d 23:59:59');

            // Fetching effort in timespan
            $registered_effort = $user->effort_in_timespan($start, $end)->get();

            // Processing effort
            $processed_effort = array();
            $total_time = 0;
            $distance = 0;
            foreach ($registered_effort as $effort) {
                // Fetching day
                $day = Carbon::parse($effort->start)->format('Y-m-d');

                // Adding or defining seconds for day
                if (array_key_exists($day, $processed_effort)) {
                    $processed_effort[$day] += $effort->effort_time_in_seconds();
                } else {
                    $processed_effort[$day] = $effort->effort_time_in_seconds();
                }

                // Adding to total time
                $total_time += $effort->effort_time_in_seconds();

                // Adding to total distance
                $dis = $effort->stats()->where('stat_type_id', 2)->first();
                // dump($dis, ->first());
                if (!is_null($dis)) {
                    $distance += (float)$dis->value;
                }
            }

            // Creating week in return data
            $time = array();
            $current = $now->startOfWeek();
            for ($i = 0; $i < 7; $i++) {
                if (array_key_exists($current->format('Y-m-d'), $processed_effort)) {
                    // $return['tiempo'][] = gmdate("H:i:s", $processed_effort[$current->format('Y-m-d')]);
                    $time[] = (float)number_format($processed_effort[$current->format('Y-m-d')] / 60, 2, '.', '');
                    if ($i != 0) {
                        $result_ten = ($time[$i - 1] * 0.10) + $time[$i - 1];
                        $result_fifteen = ($time[$i - 1] * 0.15) + $time[$i - 1];
                        $result_twenty = ($time[$i - 1] * 0.20) + $time[$i - 1];

                        $medal = null;
                        if ($time[$i] >= $result_ten) {
                            $medal = 'Bronce';

                            if ($time[$i] >= $result_fifteen) {
                                $medal = 'Plata';
                                if ($time[$i] >= $result_twenty) {
                                    $medal = 'Oro';
                                    // dd($result_twenty);
                                }
                            }
                        }
                    }
                } else {
                    $time[] = 0;
                }

                // Adding total time
                $current->addDay();
            }


            return [
                'error' => false,
                'details' => [
                    'id' => $user->id,
                    'name' => $user->full_name(),
                    'age' => (int)$age,
                    'weight' => number_format($weight, 2, '.', ''),
                    'height' => number_format($height, 2, '.', ''),
                    'tiempo' => $time,
                    'medal' => $medal ?? null
                ],
                'name_detail' => [
                    'name' => $user->name,
                    'last_name' => $user->last_name,
                    'second_last_name' => $user->second_last_name,
                ],
                'Tiempo' => number_format($total_time / 60, 2, '.', ''),
                'Distancia' => number_format($distance / 100, 2, '.', ''),
            ];
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            // User not found
            return [
                'error' => true,
                'message' => 'Usuario no encontrado'
            ];
        } catch (Exception $e) {
            // Other exceptions
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function effort_detail_bak(Request $request)
    // public function effort_detail(Request $request)
    {
        // Fetching user and parsing dates
        try {
            if (isset($request->device_id)) {
                $user = User::where('device_id', $request->device_id)->first();
                $age = '0';
                $weight = '0';
                $height = '0';
            } else {
                $user = User::find($request->user_id);
                $age = Carbon::parse($user->birthdate)->age;
                $weight = $user->weight;
                $height = $user->height;
            }

            $now = Carbon::now();
            $start = $now->startOfWeek()->format('Y-m-d');
            $end = $now->endOfWeek()->format('Y-m-d');

            if (is_null($user)) {
                return [
                    'error' => true,
                    'message' => 'Usuario no encontrado'
                ];
            }

            // Defining basic return data
            $return = [
                'id' => $user->id,
                'name' => $user->name . " " . $user->last_name,
                'age' => $age,
                'weight' => $weight,
                'height' => $height,
            ];


            if (!is_null($user->effort->whereBetween('end', [$start, $end])->first())) {
                # code...
                foreach ($user->effort->whereBetween('end', [$start, $end]) as $effort) {

                    $x = 0;
                    $beginning = Carbon::parse($effort->start);

                    $search = $effort->stats->pluck('stat_type_id')->toArray();

                    foreach ($effort->stats as $stat) {
                        if ($x == 0) {
                            if (in_array(5,  $search)) {
                                if ($stat->type->name == 'Pasos') {
                                    $return['pasos'][] = $stat->value;
                                }
                            } else {
                                $return['pasos'][] = 0;
                                $x = 1;
                            }
                        }

                        if (!array_key_exists($stat->type->name, $return)) {
                            if ($stat->type->name == 'Tiempo') {
                                $return[$stat->type->name] =  Carbon::parse($stat->value);
                            } else {
                                $return[$stat->type->name] =  $stat->value;
                            }
                        } else {
                            if ($stat->type->name == 'Tiempo') {
                                $time = Carbon::parse($stat->value);
                                $second = $time->second;
                                $return[$stat->type->name]->addSecond($second);
                                $minute = $time->minute;
                                $return[$stat->type->name]->addMinute($minute);
                                $hour = $time->hour;
                                $return[$stat->type->name]->addHour($hour);
                            } else {
                                $return[$stat->type->name] += $stat->value;
                            }
                        }
                    }
                }
                // dd($return);
                $return['Tiempo'] = $return['Tiempo']->format('H:i:s');
                if (count($return['pasos']) != 7) {
                    $res = 7 - count($return['pasos']);
                    for ($i = 0; $i < $res; $i++) {
                        $return['pasos'][] = 0;
                    }
                }
            } else {
                $stattype = StatType::pluck('name');
                foreach ($stattype as $stat) {
                    $return[$stat] = 0;
                }
                for ($i = 0; $i < 7; $i++) {
                    $return['pasos'][] = 0;
                }
            }



            return [
                'error' => false,
                'details' => $return
            ];
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }

    public function get_historial(Request $request, $no_cta)
    {
        // try {
        $soap = new SOAP('historial');
        $login_result = $soap->buscarHistorial('314121345');
        // dd($login_result);

        /* } catch (\Exception $e) {

            \Log::error('Soap Exception: ' . $e->getMessage());
            throw new \Exception('Problem with SOAP call');
        }*/

        $return = array();

        /*foreach ($login_result->LoginRedPumaResult as $key => $value) {
            $return[$key] = $value;
        }*/
        return $return;
    }

    public function ApiLogin(Request $request)
    {
        // Trying to login using loginRedPuma SOAP service
        try {
            $soap = new SOAP('login');
            $login_result = $soap->loginRedPuma($request['email'], $request['password']);
            $data = (array) $login_result->LoginRedPumaResult;


            // Decode response
            // Check if response contains CURP
            if (array_key_exists('CURP', $data)) {
                $decrypter = new Encrypter();
                $data['FechaUS'] = $decrypter->decryptMessage($data['FechaUS']);
                $data['CURP'] = $decrypter->decryptMessage($data['CURP']);
                $data['Apepat'] = $decrypter->decryptMessage($data['Apepat']);
                $data['Apemat'] = $decrypter->decryptMessage($data['Apemat']);
                $data['Nombres'] = $decrypter->decryptMessage($data['Nombres']);
                $data['FecNac'] = $decrypter->decryptMessage($data['FecNac']);
                $data['NoCuenta'] = $decrypter->decryptMessage($data['NoCuenta']);
                $data['PlantelNo'] = $decrypter->decryptMessage($data['PlantelNo']);
                $data['TipoUsr'] = $decrypter->decryptMessage($data['TipoUsr']);
                $data['Peso'] = $decrypter->decryptMessage($data['Peso']);
                $data['Talla'] = $decrypter->decryptMessage($data['Talla']);
                $data['Dependencia'] = $decrypter->decryptMessage($data['Dependencia']);
                $data['ClDGAE'] = $decrypter->decryptMessage($data['ClDGAE']);

                // \Log::info("PESO: " . $data['Peso']);
            }

            // If user is not null
            if (!is_null(User::where('CURP', $data['CURP'])->first())) {
                // Fetch user
                $user = User::where('CURP', $data['CURP'])->first();

                // Updating user device_id
                $user->update([
                    'device_id' => $request->deviceId,
                    'weight' => number_format($data['Peso'], 2, '.', ''),
                    'height' => number_format($data['Talla'], 2, '.', ''),
                ]);

                // $user = User::updateOrCreate([
                //     'email' => $request['email'],
                // ], [
                //     'device_id' => $request['deviceId'],
                //     'name' => $data['Nombres'],
                //     'last_name' => $data['Apepat'],
                //     'second_last_name' => $data['Apemat'],
                //     'birthdate' => $fecnac,
                //     'CURP' => $data['CURP'],
                //     'password' => Hash::make($request['password']),
                //     'weight' => number_format($data['Peso'], 2, '.', ''),
                //     'height' => number_format($data['Talla'], 2, '.', ''),
                //     'worker_number' => $data['NoCuenta'],
                //     'master' => $data['TipoUsr'],
                //     'campus' => $data['Dependencia'],
                //     'user_type_id' => 2,
                // ]);


                // If user has email, we update data from webservice
                // if (!is_null($user->email)) {
                //     $this->updateUserData($user);
                //     $user = User::find($user->id);
                // }

                if (array_key_exists('Eventos', $data)) {
                    $event = Event::where('noid', $data['Eventos']->InfEvento->NOID)->get()->first();

                    if (is_null($event)) {
                        $type = \EventType::byKey($data['Eventos']->InfEvento->TIPOEVENTO);
                        $event = Event::updateOrCreate(
                            ['name' => $data['Eventos']->InfEvento->NOMEVENTO, 'noid' => $data['Eventos']->InfEvento->NOID],
                            ['date' => $data['Eventos']->InfEvento->FECI, 'type' => $type->id]
                        );
                    }
                    if (is_null($user->events_user($event))) {
                        if ($data['Eventos']->InfEvento->NoCorredor != 0) {
                            $category = Category::where('cod_category', $data['Eventos']->InfEvento->NoIdOrden)->get()->first();

                            foreach ($data['Eventos']->InfEvento->InfoCat->InfoCategoria as $InfoCat) {
                                // Fetch category
                                $category = Category::updateOrCreate(
                                    ['name' => $InfoCat->Categoria, 'cod_category' => $InfoCat->NoidOrden],
                                    ['FOLini' => $InfoCat->FOLini, 'FOLfin' => $InfoCat->FOLfin]
                                );

                                // Sync event
                                $user->event()->syncWithoutDetaching(
                                    [
                                        $event->id => [
                                            'folio' => $data['Eventos']->InfEvento->NoCorredor,
                                            'category_id' => Category::where('cod_category', $InfoCat->NoidOrden)->get()->first()->id
                                        ]
                                    ]
                                );
                            }
                        }
                    }
                }
            } else {
                //dd($data['FecNac']);
                $date = $data['FecNac'];
                $fecnac = Carbon::parse($date);
                $user = User::updateOrCreate([
                    'email' => $request['email'],
                ], [
                    'device_id' => $request['deviceId'],
                    'name' => $data['Nombres'],
                    'last_name' => $data['Apepat'],
                    'second_last_name' => $data['Apemat'],
                    'birthdate' => $fecnac,
                    'CURP' => $data['CURP'],
                    'password' => Hash::make($request['password']),
                    'weight' => number_format($data['Peso'], 2, '.', ''),
                    'height' => number_format($data['Talla'], 2, '.', ''),
                    'worker_number' => $data['NoCuenta'],
                    'master' => $data['TipoUsr'],
                    'campus' => $data['Dependencia'],
                    'user_type_id' => 2,
                ]);

                \Log::info("Otro peso: " . number_format($data['Peso'], 2, '.', ''));

                // \Log::info($user);

                if (array_key_exists('Eventos', $data)) {
                    $event = Event::where('noid', $data['Eventos']->InfEvento->NOID)->get()->first();

                    if (is_null($user->events_user($event))) {
                        if ($data['Eventos']->InfEvento->NoCorredor != 0) {

                            $category = Category::where('cod_category', $data['Eventos']->InfEvento->NoIdOrden)->get()->first();

                            $user->event()->syncWithoutDetaching([$event->id => ['folio' => $data['Eventos']->InfEvento->NoCorredor, 'category_id' => $category->id]]);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $soap = new SOAP('login');

            $login_result = $soap->loginRedPuma($request['email'], $request['password']);
            $data = (array) $login_result->LoginRedPumaResult;

            // \Log::info($data);

            if (isset($data['APError'])) {
                return [
                    'error' => true,
                    'message' => $data['APError']
                ];
            }
            \Log::error('Soap Exception: ' . $e->getMessage());
            // throw new \Exception('Problem with SOAP call');
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
        // Returning LoginRedPumaResult data

        return [
            'error' => false,
            'message' => 'Registro exitoso',
            'user' => new UserResource($user),
        ];
    }

    public function result_event($event_id, $user_id)
    {
        // Fetching user, event and relation
        $user = User::find($user_id);
        $event = Event::find($event_id);
        $user_event = $user->events()->where('event_id', $event->id)->first();

        // Base response data
        $return = [
            'event' => $event->name,
            'date' => Carbon::create($event->date)->format('Y-m-d H:i'),
        ];

        // Fetching al categories
        foreach (Category::all() as $category) {
            // Fetching results for category
            foreach ($event->results_by_category($category->id) as $category_result) {
                // Fetching effort
                $effort = Effort::find($category_result->pivot->effort_id);

                // Checking if there is a registered effort for event
                if (!is_null($effort)) {
                    // Inserting result into return array
                    $return['results'][$category->name]['results'][] = [
                        'name' => $category_result->name,
                        'folio' => $category_result->pivot->folio,
                        'position' => $category_result->pivot->position,
                        'time' => $effort->effort_stat_by_type(1)->value ?? '00:00:00',
                        'distance' => $effort->effort_stat_by_type(2)->value ?? '0.0',
                    ];
                }
            }
        }

        // Appending user to result list in case its not in the first 10
        if (!is_null($user_event) && $user_event->pivot->position > 10) {
            $cat = Category::find($user_event->pivot->category_id);
            $effort = Effort::find($user_event->pivot->effort_id);

            $return['results'][$cat->name]['results'][] = [
                'name' => $user->name,
                'folio' => $user_event->pivot->folio,
                'position' => $user_event->pivot->position,
                'time' => $effort->effort_stat_by_type(1)->value ?? '00:00:00',
                'distance' => $effort->effort_stat_by_type(2)->value ?? '0.0',
            ];
        }

        // Checking if the event has any results, otherwise we alert the app
        if (!array_key_exists('results', $return)) {
            $return['error'] = true;
            $return['error_message'] = 'Actualmente no existen resultados';
        } else {
            $return['error'] = false;
            $return['error_message'] = '';
        }
        //  else {
        //     $return['results']['error'] = false;
        // }

        // Returning data
        return $return;
    }

    public function resetPassword(Request $request)
    {
        $soap = new SOAP('login');
        $reset_result = $soap->recuperarPassword($request['email']);
        //dd($reset_result);
        $data = (array) $reset_result->OlvContraResult;
        return [
            'error' => $data['MError'],
            'message' => $data['APError']
        ];
        // dd($data);
    }


    // public function result_event($event, $user)
    // {

    //     $return = array();
    //     $user = User::find($user);
    //     $you = $user->events_user($event);
    //     // dd($you);
    //     //$event = Event::find($event);
    //     $event = Event::find($event); // or get() or whatever
    //     $effort_user = Effort::find($you->pivot->effort_id);

    //     // dd($event, $you, $effort_user);

    //     // Verify if there is data to show
    //     if (is_null($effort_user) || is_null($effort_user->stats)) {
    //         return [
    //             'error' => true,
    //             'error_message' => "No hay resultados por mostrar",
    //         ];
    //     }

    //     foreach ($effort_user->stats as $stat_user) {
    //         if ($stat_user->type->name == 'Tiempo') {
    //             $time_user = $stat_user->value;
    //         } elseif ($stat_user->type->name == 'Distancia') {
    //             $distance_user = $stat_user->value;
    //         }
    //     }

    //     // dd($user->events_user($event));
    //     // dd($event->users_category($type)->take(10));
    //     // dd($event->users->pivot->sortBy('pivot_position'));
    //     // dd($event->result->sortBy('position')->take(10));
    //     $categories = Category::all();
    //     $return['error'] = false;
    //     $return['event_name'] = $event->name;
    //     $return['distance'] = $distance_user;
    //     $return['date'] = $event->date;
    //     $return['time'] = $time_user;

    //     if (!is_null($event->users->first())) {
    //         foreach ($categories as $category) {
    //             //dd($event->users_category($category->id)->take(10));
    //             foreach ($event->users_category($category->id)->take(10) as $result) {
    //                 $effort = Effort::find($result->pivot->effort_id);
    //                 //dd($result->pivot->effort_id);
    //                 foreach ($effort->stats as $stat) {
    //                     if ($stat->type->name == 'Tiempo') {
    //                         $time = $stat->value;
    //                     } elseif ($stat->type->name == 'Distancia') {
    //                         $distance = $stat->value;
    //                     }
    //                 }

    //                 $name = $result->name . " " . $result->last_name . " " . $result->second_last_name;
    //                 $return[$category->name]['result'][] = [
    //                     'position' => $result->pivot->position,
    //                     'name' => $name,
    //                     'folio' => $result->pivot->folio,
    //                     'time' => $time,
    //                     'distance' => $distance
    //                 ];
    //             }
    //             //  dd($return);

    //             if (!is_null($user->events_user_categorie($category->id))) {
    //                 if (is_null($user->events_user_position($event))) {
    //                     if (!is_null($you)) {
    //                         $name = $user->name . " " . $user->last_name . " " . $user->second_last_name;
    //                         $effort = Effort::find($you->pivot->effort_id);

    //                         foreach ($effort->stats as $stat) {
    //                             if ($stat->type->name == 'Tiempo') {
    //                                 $time = $stat->value;
    //                             } elseif ($stat->type->name == 'Distancia') {
    //                                 $distance = $stat->value;
    //                             }
    //                         }
    //                         $return[$category->name]['result'][] = [
    //                             'position' => $you->pivot->position,
    //                             'name' => $name,
    //                             'folio' => $you->pivot->folio,
    //                             'time' => $time,
    //                             'distance' => $distance
    //                         ];
    //                     }
    //                 }
    //             }
    //         }
    //     } else {
    //         $return['result'] = null;
    //     }


    //     return $return;
    // }

    public function register(Request $request)
    {
        try {
            // \Log::info($request);
            $soap = new SOAP('login');
            $registre_result = $soap->registro($request);
            $data = (array) $registre_result->InUSRResult;
            // \Log::info($data);

            return [
                'error' => $data['Merror'],
                'message' => $data['ApMsg'],
            ];
        } catch (\Throwable $th) {
            // \Log::info($th->getMessage());
            return [
                'error' => true,
                'message' => 'Hubo un error con el registro, por favor intente nuevamente más tarde.'
            ];
        }
    }

    public function videos()
    {
        $videos = video::orderBy('order')->get();
        if (!is_null($videos->first())) {
            $data = array();

            foreach ($videos as $video) {
                $data[] = [
                    'id' => $video->id,
                    'title' => $video->title,
                    'playlist_id' => $video->playlist_id,
                    'order' => $video->order,
                ];
            }
            return [
                'error' => false,
                'videos' => $data
            ];
        } else {
            return [
                'error' => true,
                'message' => 'No se pudo obtener la información requerida'
            ];
        }
    }

    public function content()
    {
        $contents = Content::all();
        if (!is_null($contents->first())) {
            # code...
            $data = array();

            foreach ($contents as $content) {
                $data[] = [
                    'id' => $content->id,
                    'key' => $content->key,
                    'content' => $content->content,
                    'lat' => $content->location->first()->lat ?? null,
                    'long' => $content->location->first()->long ?? null,
                ];
            }
            return [
                'error' => false,
                'programs' => $data
            ];
        } else {
            return [
                'error' => true,

                'message' => 'No se pudo obtener la información requerida'
            ];
        }
    }
    public function friends(Request $request)
    {
        $friends = User::friend($request['id_user'], 'approved');
        $friends_pending = User::friend($request['id_user'], 'resquest');

        if (is_null($friends['friends1']->first()) && is_null($friends['friends2']->first())) {

            return [
                'error' => false,
                'pendigs' => count($friends_pending['friends2']),
                'message' => 'No tienes amigos',
                'friends' => [],
            ];
        }

        $data = array();
        // foreach ($friends['friends1'] as $freind) {
        //     $data[] = [
        //         'id' => $freind->id,
        //         'name' => $freind->name . ' ' . $freind->last_name . ' ' . $freind->second_last_name,
        //         'email' =>  $freind->email,
        //     ];
        // }

        foreach ($friends['friends2'] as $freind) {
            $data[] = [
                'id' => $freind->id,
                'name' => $freind->name . ' ' . $freind->last_name . ' ' . $freind->second_last_name,
                'email' =>  $freind->email
            ];
        }

        $user = User::find($request['id_user']);

        $details = [

            'Me gusta' => $user->friends_reactions2()->wherePivot('type', '=', 'like')->count(),
            'Admiracion' => $user->friends_reactions2()->wherePivot('type', '=', 'admiration')->count(),
            'Orgullo' => $user->friends_reactions2()->wherePivot('type', '=', 'pride')->count(),
            'Sigue mejorando' => $user->friends_reactions2()->wherePivot('type', '=', 'improve')->count(),

        ];

        return [
            'error' => false,
            'pendigs' => count($friends_pending['friends2']),
            'friends' => $data,
            'details' => $details,
            'last' => $user->friends_reactions2->last()->name ?? null,
            'type' => $user->friends_reactions2->last()->pivot->type ?? null,
        ];
    }

    public function friends_pending(Request $request)
    {
        $friends = User::friend($request->id_user, 'resquest');
        if (is_null($friends['friends2']->first())) {
            return [
                'error' => false,
                'pendigs' => count($friends['friends2']),
                'message' => 'No hay solicitudes pendientes',
                'friends' => ''
            ];
        }


        $data = array();
        foreach ($friends['friends2'] as $freind) {
            $data[] = [
                'id' => $freind->id,
                'name' => $freind->name . ' ' . $freind->last_name . ' ' . $freind->second_last_name,
                'email' =>  $freind->email
            ];
        }

        return [
            'error' => false,
            'pendigs' => count($friends['friends2']),
            'friends' => $data
        ];
    }

    public function friends_Mypending(Request $request)
    {
        $friends = User::friend($request->id_user, 'resquest');

        if (is_null($friends['friends1']->first())) {
            return [
                'error' => false,
                'pendigs' => count($friends['friends2']),
                'message' => 'No hay solicitudes pendientes',
                'friends' => ''
            ];
        }


        $data = array();
        foreach ($friends['friends1'] as $freind) {
            $data[] = [
                'id' => $freind->id,
                'name' => $freind->name . ' ' . $freind->last_name . ' ' . $freind->second_last_name,
                'email' =>  $freind->email
            ];
        }

        return [
            'error' => false,
            'pendigs' => count($friends['friends2']),
            'friends' => $data
        ];
    }


    public function add_friend(Request $request)
    {
        $user = User::find($request->id_user);
        $friend = User::where('email', $request->email)->first();

        if (is_null($friend)) {
            return [
                'error' => true,
                'message' => 'No se encontro el usuario de este correo electronico'
            ];
        }

        if (!is_null($user->friends_plus1()->wherePivot('friend_id', $friend->id)->first()) || !is_null($user->friends_plus2()->wherePivot('friend_id', $friend->id)->first())) {
            return [
                'error' => true,
                'message' => 'La solicitud ya fue enviada anteriormente'
            ];
        }



        $user->friends_plus1()->attach($friend->id, ['type' => 'resquest']);

        return [
            'error' => false,
            'message' => 'Solicitud enviada.',

        ];
    }


    public function change_status_friend(Request $request)
    {
        $user = User::find($request['id_user']);
        $userFriend = User::find($request['id_friend']);
        $user->friends_plus2()->syncWithoutDetaching([$userFriend->id => ['type' => $request['type']]]);
        $userFriend->friends_plus2()->syncWithoutDetaching([$user->id => ['type' => $request['type']]]);

        if ($request['type'] == 'approved') {
            return [
                'error' => false,
                'aceptado' => true,
                'message' => 'Solicitud aceptada.',

            ];
        } else {
            return [
                'error' => false,
                'aceptado' => false,
                'message' => 'Solicitud rechazada.',

            ];
        }
    }

    public function reactions(Request $request)
    {
        $user = User::find($request['id_user']);
        $user->friends_reactions()->syncWithoutDetaching([$request['id_friend'] => ['type' => $request['type']]]);
        return [
            'error' => false,
            'message' => 'Reaccion enviada',

        ];
    }

    public function friends_details(Request $request)
    {
        $user = User::find($request['id_friend']);
        $friends = User::friend($request->id_user, 'resquest');


        $data = array();

        $data[] = [

            'Me gusta' => $user->friends_reactions2()->wherePivot('type', '=', 'like')->count(),
            'Admiracion' => $user->friends_reactions2()->wherePivot('type', '=', 'admiration')->count(),
            'Orgullo' => $user->friends_reactions2()->wherePivot('type', '=', 'pride')->count(),
            'Sigue mejorando' => $user->friends_reactions2()->wherePivot('type', '=', 'improve')->count(),

        ];


        return [
            'error' => false,
            'pendigs' => count($friends['friends2']),
            'details' => $data,
            'last' => $user->friends_reactions2->last()->name ?? null,
            'type' => $user->friends_reactions2->last()->pivot->type ?? null,
        ];
    }

    public function create_team(Request $request)
    {

        if (is_null(Team::where('name', $request['name'])->where('event_id', $request['event_id'])->first())) {
            $request->request->add([
                'password' => bcrypt($request->password),
            ]);

            $team = Team::create($request->except('_token', 'id_user'));
            $team->user()->attach($request['id_user']);
            return [
                'error' => false,
                'message' => 'Equipo creado crrectamente',
            ];
        }

        return [
            'error' => true,
            'message' => 'El equipo ya fue creado anteriormente para este evento',
        ];
    }

    public function add_user_team(Request $request)
    {
        $team = Team::where('name', $request['name'])->where('event_id', $request['event_id'])->first();
        // dd($team->password, bcrypt($request['password']));
        if (is_null($team)) {
            return [
                'error' => true,
                'message' => 'No se encontro el equipo',
            ];
        } elseif (Hash::check($request['password'], $team->password)) {

            $team->user()->attach($request['id_user']);
            return [
                'error' => false,
                'message' => 'Te has unido al equipo ' . $team->name,
            ];
        } else {
            return [
                'error' => true,
                'message' => 'Contraseña incorrecta',
            ];
        }
    }

    public function list_user_team(Request $request)
    {
        $teams = Team::where('event_id', $request['event_id'])->get();
        foreach ($teams as $team) {
            $user_team = $team->user()->wherePivot('user_id', $request['id_user'])->first();
            if (!is_null($user_team)) {
                $team_id = $user_team->pivot->team_id;
            }
        }

        $team_user = Team::find($team_id);
        $date = explode(' ', $team_user->event->date);
        $data = array();
        foreach ($team_user->user as $user) {
            $beginning = Carbon::parse($user->pivot->start);
            $ending = Carbon::parse($user->pivot->end);
            $min = $beginning->diffInMinutes($ending);
            $diff = Carbon::parse('00:00:00');
            $diff = $diff->addMinute($min)->format('H:i:s');
            $data[] = [
                'id' => $user->id,
                'name' => $user->full_name(),
                'time' => $diff,

            ];
        }

        return [
            'error' => false,
            'event' => $team_user->event->name,
            'date' => $date[0],
            'time' => $date[1],
            'team' => $team_user->name,
            'team_id' => $team_user->id,
            'users' => $data,
        ];
    }

    public function add_result(Request $request)
    {
        $team = Team::find($request['team_id']);

        $team->user()->syncWithoutDetaching([$request['id_user'] => ['start' => $request['start'], 'end' => $request['end']]]);

        return [
            'error' => false,
            'message' => 'Tu tiempo fue registrado con exito'
        ];
    }

    /**
     * Register user to even using soap dgdu
     * @param user_id : id user
     * @param category_id: id category
     * @param event_id: id event
     */
    public function registerUserEvent(Request $request)
    {
        // Trying to register evente using loginRedPuma SOAP service
        try {
            $user = User::find($request['user_id']);
            $category = Category::find($request['category_id']);
            $event = Event::find($request['event_id']);
            $soap = new SOAP('registroEvento');

            $result = $soap->registerUserEvent([
                'lfi' => $category->FOLini, //limite inferior de la carrera
                'lff' => $category->FOLfin, //limite superior de la carrera
                'noid' => $event->noid, //número del evento
                'noidorden' => $category->cod_category, //categoria seleccionada
                'curp' => $user->CURP, //curp
            ]);


            $data = (array) $result->GuardaCorredorResult;
            // \Log::info('Respuesta DGDU');
            // \Log::info((array)$result);
            //if there was no error in the answer
            if (!$data['Merror']) {
                $user->events()->attach($event->id, ['category_id' => $category->id]);
            }

            // Returning result data
            $decrypter = new Encrypter();
            $data['NoCorredor'] = $decrypter->decryptMessage($data['NoCorredor']);
            return [
                'error' => $data['Merror'],
                'message' => $data['Msg'],
                'corredor' => $data['NoCorredor']
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo registrar al usuario al evento'
            ];
        }
    }

    /**
     * Get all Categories
     * @return array of categories
     */
    public function getCategories()
    {
        $data = array();
        foreach (Category::all() as $category) {
            $data[] = [
                'id' => $category->id,
                'name' => $category->name,
                'cod' => $category->cod_category,
            ];
        }

        return [
            'error' => false,
            'categories' => $data
        ];
    }

    public function updateUserData(Request $request)
    {
        // Creating SOAP client
        $soap = new SOAP('info_usuarios');

        // Calling service
        $result = $soap->fetchUserData($request->email, $request->password);
        $data = (array)$result->ActDatosResult;

        // return $data;


        // Returning error response
        if ($data['MError']) {
            return [
                'error' => false,
                'error_message' => $data['APError']
            ];
        }

        // Parsing response data
        $decrypter = new Encrypter();
        $data['Nombres'] = $decrypter->decryptMessage($data['Nombres']);
        $data['Apepat'] = $decrypter->decryptMessage($data['Apepat']);
        $data['Apemat'] = $decrypter->decryptMessage($data['Apemat']);
        $data['ESTATURA'] = $decrypter->decryptMessage($data['ESTATURA']);
        $data['PESO'] = $decrypter->decryptMessage($data['PESO']);

        // Updating user data
        $user = User::findByEmail($request->email);
        $user->update([
            'name' => $data['Nombres'],
            'last_name' => $data['Apepat'],
            'second_last_name' => $data['Apemat'],
            // 'CURP' => $data['CURP'],
            // 'worker_number' => $data['NOCUENTA'],
            'height' => $data['ESTATURA'],
            'weight' => $data['PESO'],
        ]);

        // Returning response
        return [
            'error' => false,
            'user_data' => [
                'name' => $user->name,
                'last_name' => $user->last_name,
                'second_last_name' => $user->second_last_name,
                'height' => $user->height,
                'weight' => $user->weight,
            ],
        ];




        // Simulating response
        // $user = User::find(3);
        // $result = [
        //     'Nombres' => 'DIEGO ' . mt_rand(0, 99),
        //     'ESTATURA' => '1.' . mt_rand(0, 99), // 1.82
        //     'PESO' => mt_rand(80, 89),
        // ];
        // $user->update([
        //     'name' => $result['Nombres'],
        //     // 'last_name' => $result['Apepat'],
        //     // 'second_last_name' => $result['Apemat'],
        //     // 'CURP' => $result['CURP'],
        //     // 'worker_number' => $result['NOCUENTA'],
        //     'height' => $result['ESTATURA'],
        //     'weight' => $result['PESO'],
        // ]);
    }
}
