<?php

namespace App\Http\Controllers;

use App\Models\ResultEvent;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ResultEventImport;
use App\Models\User;
use App\Models\Type;
use App\Models\Category;
use App\Models\EventResult;
use File;

class ResultEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        return view('events.import_result', [
            'categories' => Category::all(),
            'event_id' => $id,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('dashboard'), 'active' => false],
                ['label' => 'Eventos', 'url' => route('events.index'), 'active' => true],
            ]
        ]);
    }



    public function import(Request $request, $id)
    {
        //dd($request->all());

        foreach ($request->except('_token') as $key => $value) {
            // Verify file extension
            $extension = $request->file($key)->getClientOriginalExtension();
            if ($extension != 'tsv') {
                return back()->with('error', 'El archivo debe tener extensión .tsv');
            }


            // Parsing lines
            $lines = explode(PHP_EOL, File::get($request->$key));
            array_shift($lines);
            $type = Category::find($key);
            foreach ($lines as $line) {

                // Parsing line
                $line = explode("\t", str_replace("\r", "", $line));
                $user = User::where('name', $line[0])->where('last_name', $line[1])->where('second_last_name', $line[2])->get()->first();
                //dd($user);
                $user->events()->syncWithoutDetaching([$id => ['position' => $line[5],  'folio' => $line[3]]]);
                // Creating accreditation

            }
        }


        return back()->with('success', 'Archivo importado correctamente');
    }
}
