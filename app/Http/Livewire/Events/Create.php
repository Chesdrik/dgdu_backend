<?php

namespace App\Http\Livewire\Events;

use Livewire\Component;
use App\Models\Event;
use App\Models\EventType;

class Create extends Component
{
    public $name;
    public $start;
    public $end;
    public $max_users;
    public $active;
    public $noid;
    public $eventType;
    public $eventTypeId;
    public $eventTypesArray;
    public $effortTypesArray;
    public $effortTypeId;

    public function submit()
    {
        // Validating input
        $this->validate();

        // Create event
        $event = Event::create([
            'name' => $this->name,
            'start' => $this->start,
            'end' => $this->end,
            'max_users' => $this->max_users,
            'active' => 0,
            'noid' => $this->noid,
            'event_type_id' => $this->eventTypeId,
            'effort_type_id' => $this->effortTypeId,

        ]);

        // Redirecting to event detail
        return redirect()->route('events.show', $event->id);
    }


    public function mount($eventTypesArray, $effortTypesArray)
    {
        $this->eventTypesArray = $eventTypesArray;
        $this->effortTypesArray = $effortTypesArray;
    }

    public function render()
    {
        if (!is_null($this->eventTypeId) && $this->eventTypeId != "0") {
            $this->eventType = EventType::find($this->eventTypeId);
        } else {
            $this->eventType = null;
        }

        return view('livewire.events.create');
    }

    // Validation rules
    public function rules()
    {
        // Base validation rules
        $rules = [
            'name' => 'required',
            'start' => 'required',
            'end' => 'required',
            // 'noid' => 'required|numeric',
            'eventTypeId' => 'required|numeric|gt:0',
            'effortTypeId' => 'required|numeric|gt:0',
        ];

        // If it's a team event, we validate max_users
        if ($this->eventType->team) {
            $rules['max_users'] = 'sometimes|required';
        }

        // Returning rules
        return $rules;
    }
    //

    // Validation messages
    public function messages()
    {
        return [
            'name.required' => 'Se debe ingresar un nombre para el conjunto',
            'start.required' => 'Es necesario ingresar la fecha de inicio del evento',
            'end.required' => 'Es necesario ingresar la fecha de fin del evento',
            'max_users.required' => 'Es necesario ingresar el número máximo de usuarios',
            'eventTypeId.required' => 'Es necesario seleccionar el tipo de evento',
            'eventTypeId.numeric' => 'Es necesario seleccionar el tipo de evento',
            'eventTypeId.gt' => 'Es necesario seleccionar el tipo de evento',
            'effortTypeId.required' => 'Es necesario seleccionar el tipo de esfuerzo para el evento',
            'effortTypeId.numeric' => 'Es necesario seleccionar el tipo de esfuerzo para el evento',
            'effortTypeId.gt' => 'Es necesario seleccionar el tipo de esfuerzo para el evento',
        ];
    }
}
