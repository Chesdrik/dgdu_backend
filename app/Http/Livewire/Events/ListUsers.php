<?php

namespace App\Http\Livewire\Events;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Team;

class ListUsers extends Component
{
    // Pagination
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $event;
    public $team;
    public $teams;

    public function mount()
    {
        $this->team = $this->event->event_type->team ? true : false;
        if ($this->team) {
            $this->teams = Team::where('event_id', $this->event->id)->pluck('name', 'id');
        }
    }

    public function render()
    {
        $users = $this->event->users()->orderBy('team_id')->paginate(10);

        return view('livewire.events.list-users', [
            'users' => $users
        ]);
    }
}
