<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'second_last_name' => $this->second_last_name,
            'birthdate' => $this->birthdate,
            'CURP' => $this->CURP,
            'gender' => $this->gender,
            'email' => $this->email,
            'weight' => number_format($this->weight, 2, '.', ''),
            'height' => number_format($this->height, 2, '.', ''),
            'worker_number' => $this->worker_number,
            'master' => $this->master,
            'campus' => $this->campus,
            'career' => $this->career,
        ];
    }
}
