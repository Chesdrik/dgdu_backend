<?php

function pretty_date($date)
{
    return substr($date, 0, 16);
}

function pretty_short_date($date)
{
    return substr($date, 0, 10);
}

function pretty_short_hour($hour)
{
    return substr($hour, -8, 5);
}

function pretty_type($type)
{
    switch ($type) {
        case 'like':
            return 'Me gusta';
            break;
        case 'admiration':
            return 'Adminracion';
            break;
        case 'pride':
            return 'Orgullo';
            break;
        case 'improve':
            return 'Sigue mejorando';
            break;
        default:
            return 'error';
            break;
    }
}

function delte_accent($name)
{

    //Codificamos la cadena en formato utf8 en caso de que nos de errores


    //Ahora reemplazamos las letras
    $name = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $name
    );

    $name = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $name
    );

    $name = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $name
    );

    $name = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $name
    );

    $name = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $name
    );

    $name = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $name
    );

    return $name;
}

// Comparison function
function date_compare($element1, $element2)
{
    $date1 = strtotime($element1['date']);
    $date2 = strtotime($element2['date']);
    return $date2 - $date1;
}
