<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Effort extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'user_id', 'effort_type_id', 'event_id', 'start', 'end'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function effort_type()
    {
        return $this->belongsTo('App\Models\EffortType');
    }

    public function effort_gps_locations()
    {
        return $this->hasMany('App\Models\EffortGpsLocation');
    }

    public function stats()
    {
        return $this->hasMany('App\Models\Stat');
    }

    public function effort_stat_by_type($stat_type_id)
    {
        return $this->stats()->where('stat_type_id', $stat_type_id)->first();
    }

    // public function effort_stats_time()
    // {
    //     return $this->effort_stat_by_type(1);
    // }

    // public function effort_stats_distance()
    // {
    //     return $this->effort_stat_by_type(2);
    // }

    public function effort_time_in_seconds()
    {
        $start = Carbon::parse($this->start);
        $end = Carbon::parse($this->end);

        return $end->diffInSeconds($start);
    }

    public function effort_time_in_minutes()
    {
        return round($this->effort_time_in_seconds() / 60, 2, PHP_ROUND_HALF_DOWN);
    }

    public function pretty_timestamps()
    {
        $start = Carbon::parse($this->start);
        $end = Carbon::parse($this->end);

        // return $start->format('Y');
        if ($start->format('Y-m-d') != $end->format('Y-m-d')) {
            // Date change
            return $start->format('Y-m-d H:i') . ' -> ' . $end->format('Y-m-d H:i');
        } else {
            // Same day
            return $start->format('Y-m-d H:i') . ' -> ' . $end->format('H:i');
        }
    }
}
