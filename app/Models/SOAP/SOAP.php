<?php

namespace App\Models\SOAP;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\UserType;
use App\Models\Encrypter;

class SOAP extends Model
{
    use HasFactory;

    // Creates basic SOAP agent
    public function __construct($service_type)
    {
        // Basic agent options
        $this->options = array(
            'soap_version' => "SOAP_1_1",
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => "WSDL_CACHE_MEMORY",
            'connection_timeout' => 25,
            'style' => "SOAP_RPC",
            'use' => "SOAP_ENCODED",

        );

        // \Log::info("service type: $service_type");

        // Defining URL
        if (in_array($service_type, ['evento', 'historial'])) {
            $url = 'https://redpuma.unam.mx/Wsrepre/repre.asmx';
        } else {
            $url = 'https://sistemadeporte.unam.mx/WSDGDU21/WebS_Consumo.asmx?WSDL';
            // $url = 'https://sistemadeporte.unam.mx/WSDGDU21/WebS_Consumo.asmx';
        }

        // \Log::info($url);

        // Defining client
        $this->client = new \SoapClient($url, $this->options);
        // \Log::info($this->client);
    }

    // Basic service provider
    public function provider($method, $query)
    {
        try {
            // \Log::info("Execute $method with query");

            // Defining object response
            $response = $this->client->$method($query);
            // \Log::info(json_encode($response));
        } catch (\Exception $e) {
            // Checking if soap encoding exception
            // if ($e->getMessage() == "SOAP-ERROR: Encoding: Violation of encoding rules") {
            //     \Log::info("Catched but continue...");
            //     \Log::info(gettype($this->client->__getLastResponse()));
            //     \Log::info($this->client->__getLastResponse());


            //     $response = simplexml_load_string($this->client->__getLastResponse());
            //     dd($response);
            //     \Log::info($response);
            //     // $json = json_encode($xml);
            // } else {
            // \Log::info("Error in response");
            // \Log::info($e);

            // // Logging error
            \Log::debug('Soap Exception: ' . $e->getMessage());

            // \Log::info($this->client->__getLastResponse());

            // Throwing exception
            throw new \Exception('Problem with SOAP call');
            // }
        }

        return $response;
    }

    /**
     * Method that pull user information
     */
    public function fetchUserData($email, $password)
    {
        // Encrypt data
        $encrypter = new Encrypter();
        $email = $encrypter->setMessage($email);
        $password = $encrypter->setMessage($password);

        // Making request
        return $this->provider('ActDatos', [
            'email' => $email,
            'contrase' => $password
        ]);
    }

    // loginRedPuma service
    public function loginRedPuma($email, $password)
    {
        $encrypter = new Encrypter();
        $email = $encrypter->setMessage($email);
        $password = $encrypter->setMessage($password);

        $request = $this->provider('LoginRedPuma', [
            'Email' => $email,
            'Contrasena' => $password
        ]);

        return $request;
    }

    // buscarHistorial service
    public function buscarHistorial($no_cta)
    {
        return $this->provider('BuscarHistorial', [
            'NOCTA' => $no_cta
        ]);
    }

    public function leeEvento($curp, $password)
    {
        // Encrypt data
        $encrypter = new Encrypter();
        $curp = $encrypter->setMessage($curp);
        $password = $encrypter->setMessage($password);

        // \Log::info([
        //     'CURP' => $curp,
        //     'contrase' => $password,
        // ]);

        // Making request
        return $this->provider('LeeEventos', [
            'CURP' => $curp,
            'contrase' => $password,
        ]);
    }

    public function recuperarPassword($email)
    {
        // Encrypt data
        $encrypter = new Encrypter();
        $email = $encrypter->setMessage($email);

        // Making request
        return $this->provider('OlvContra', [
            'CorreoUsr' => $email
        ]);
    }

    public function registro($request)
    {
        // Encrypt data
        $encrypter = new Encrypter();
        $name = $encrypter->setMessage(strtoupper(delte_accent($request['name'])));
        $last_name = $encrypter->setMessage(strtoupper(delte_accent($request['last_name'])));
        $second_last_name = $encrypter->setMessage(strtoupper(delte_accent($request['second_last_name'])));
        $fn = $encrypter->setMessage((string)$request['birthday']);
        $curp = $encrypter->setMessage(strtoupper($request['CURP']));
        $email = $encrypter->setMessage($request['email']);
        $type = UserType::find($request['user_type_id']);
        $type = $encrypter->setMessage($type->name);

        // Making request
        return $this->provider('InUSR', [
            'Nom' => $name,
            'Ap' => $last_name,
            'Am' => $second_last_name,
            'Fn' => $fn,
            'Curp' => $curp,
            'Mail' => $email,
            'Tus' => $type,
        ]);
    }
    /**
     * Method that allows registering a user to an event
     */
    public function registerUserEvent($request)
    {
        // Encrypt data
        $encrypter = new Encrypter();
        $lfi = $encrypter->setMessage($request['lfi']);
        $lff = $encrypter->setMessage($request['lff']);
        $noid = $encrypter->setMessage($request['noid']);
        $noidorden = $encrypter->setMessage($request['noidorden']);
        $curp = $encrypter->setMessage($request['curp']);

        // Request
        return $this->provider('GuardaCorredor', [
            'lfi' => $lfi,
            'lff' => $lff,
            'NOID' => $noid,
            'NOIDORDEN' => $noidorden,
            'CURP' => $curp,
        ]);
    }
}
