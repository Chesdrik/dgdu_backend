<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use \Carbon\Carbon;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'start', 'end', 'active', 'noid', 'type', 'event_type_id', 'max_users', 'current_users'
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('effort_id', 'category_id', 'position', 'folio', 'team_id')->withTimestamps();
    }

    public function event_type()
    {
        return $this->belongsTo('App\Models\EventType');
    }

    public function results()
    {
        return $this->users()->orderBy('position', 'ASC')->take(10);
    }

    public function results_by_category($category_id)
    {
        return $this->users()->where('event_user.category_id', $category_id)->orderBy('position', 'ASC')->take(10)->get();
    }

    public function users_category($category)
    {
        return $this->users()->wherePivot('category_id', $category)->orderBy('position')->get();
    }

    public static function past_events()
    {
        return Event::where('end', '<=', Carbon::now())
            ->orderBy('end')
            ->get();
    }

    public static function upcoming_events()
    {
        return Event::where('end', '>=', Carbon::now())
            // ->where('active', 1)
            ->orderBy('start')
            ->get();
    }
}
