<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Event;

class EventType extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = [
        'id', 'name', 'key', 'team', 'type'
    ];

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public static function event_types_array()
    {
        return EventType::orderBy('name')->pluck('name', 'id')->toArray();
    }

    public static function byKey($key)
    {
        return EventType::where('key', $key)->first();
    }
}
