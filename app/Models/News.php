<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Vedmant\FeedReader\Facades\FeedReader;
use \Carbon\Carbon;

class News extends Model
{
    use HasFactory;
    protected $table = 'news';
    protected $fillable = [
        'id', 'title', 'content', 'date', 'img', 'active'
    ];
    public $timestamps = false;

    public function path()
    {
        return 'news/' . $this->id . '/';
    }

    public function api_img()
    {
        return 'storage/' . $this->path() . $this->img;
    }

    public function base_path()
    {
        return 'news/' . $this->id;
    }

    public function img_path($full = true)
    {
        if ($full) {
            return url('storage/' . $this->base_path() . '/' . $this->img);
        } else {
            return '/public/' . $this->base_path() . '/' . $this->img;
        }
    }

    public static function activeNews()
    {
        return News::where('active', 1)->orderBy('date', 'DESC')->get();
    }

    public static function newsForApp()
    {
        $news_array = [];

        // Appending RSS news
        foreach (News::rssNews()->get_items() as $rss_news) {
            $date = Carbon::createFromFormat('j F Y, g:i a', $rss_news->get_date());

            $news_array[] = [
                'title' => $rss_news->get_title(),
                'content' => trim($rss_news->get_content()),
                'link' => $rss_news->get_link(),
                'date' => $date->format('Y-m-d H:i'),
                // 'url' => $rss_news->get_img_noticia(),
                'image' => $rss_news->data['child']['']['img_noticia'][0]['data'],

            ];
        }

        // Appending system news
        foreach (News::activeNews() as $news) {
            // $img = file_get_contents($news->img_path());

            $news_array[] = [
                'title' => $news->title,
                'content'  => $news->content,
                'date' => pretty_date($news->date),
                'image' => $news->img_path(),
                // 'image' => base64_encode($img),
            ];
        }

        // Sorting news
        usort($news_array, 'date_compare');

        // Returning data
        return $news_array;
    }


    public static function rssNews()
    {
        return FeedReader::read('https://deporte.unam.mx/rss.php');
    }
}
