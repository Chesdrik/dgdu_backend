<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	use HasFactory;

    protected $fillable = ['content_id', 'lat', 'long'];
    public $timestamps = false;

    public function content(){
    	return $this->belongsTo('App\Models\Content');
    }
}
