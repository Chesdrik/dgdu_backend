<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\User;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type_id',
        'name',
        'last_name',
        'second_last_name',
        'CURP',
        'username',
        'password',
        'email',
        'birthdate',
        'gender',
        'weight',
        'height',
        'worker_number',
        'campus',
        'career',
        'master',
        'device_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * The events that belong to the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    // public function events(): BelongsToMany{
    //     return $this->belongsToMany(Event::class);
    // }

    public function events()
    {
        return $this->belongsToMany(Event::class)->withPivot('effort_id', 'category_id', 'position', 'folio', 'team_id', 'created_at', 'updated_at')->withTimestamps();
    }

    public function events_user_position($event)
    {
        return $this->events()->wherePivot('event_id', $event)->wherePivotIn('position', [1, 10])->first();
    }

    public function events_user_category($category)
    {
        return $this->events()->wherePivot('category_id', $category)->first();
    }

    public function events_user($event)
    {
        return $this->events()->wherePivot('event_id', $event)->first();
    }

    public function event_array()
    {
        return $this->events()->pluck('event_id')->toArray();
    }

    public function event()
    {
        return $this->belongsToMany(Event::class);
    }

    public function type()
    {
        return $this->hasOne('App\Models\UserType', 'id', 'user_type_id');
    }

    public function effort()
    {
        return $this->hasMany('App\Models\Effort');
    }

    public function effort_in_timespan($start, $end)
    {
        return $this->hasMany('App\Models\Effort')->whereBetween('start', [$start, $end])->orderBy('start');
    }

    public function full_name()
    {
        return $this->name . " " . $this->last_name;
    }


    public function APIprofile()
    {
        $return = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,

        ];

        return $return;
    }

    public static function admins()
    {
        return User::orderBy('name')->where('user_type_id', 1)->get();
    }

    public static function appUsers()
    {
        return User::orderBy('name')->where('user_type_id', 2)->get();
    }

    public static function findByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function friends_plus1()
    {
        return $this->belongsToMany('App\Models\User', 'user_friends', 'user_id', 'friend_id');
    }

    public function friends_plus2()
    {
        return $this->belongsToMany('App\Models\User', 'user_friends', 'friend_id', 'user_id')->withPivot('type');
    }

    public function friends_type1($type)
    {
        return $this->belongsToMany('App\Models\User', 'user_friends', 'user_id', 'friend_id')->withPivot('type')->wherePivot('type', $type);
    }

    public function friends_type2($type)
    {
        return $this->belongsToMany('App\Models\User', 'user_friends', 'friend_id', 'user_id')->withPivot('type')->wherePivot('type', $type);
    }


    public static function friend($id, $type)
    {

        $user = User::find($id);

        //dd($this->friends_plus1);
        $friends = array();
        $friends['friends1'] = $user->friends_type1($type)->get();
        $friends['friends2'] = $user->friends_type2($type)->get();

        return $friends;
    }

    public function friends_reactions()
    {
        return $this->belongsToMany('App\Models\User', 'reactions', 'user_id', 'friend_id')->withPivot('type');
    }

    public function friends_reactions2()
    {
        return $this->belongsToMany('App\Models\User', 'reactions', 'friend_id', 'user_id')->withPivot('type');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    public function getFullNameAttribute()
    {
        return ucwords($this->name . " " . $this->last_name . " " . $this->second_last_name);
    }
}
