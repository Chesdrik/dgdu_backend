<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventResult extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'event_id', 'categories_id', 'position', 'folio', 'time', 'speed'];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
