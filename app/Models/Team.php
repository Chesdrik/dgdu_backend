<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Team extends Model
{
    use HasFactory;
    protected $fillable = [
        'event_id', 'name', 'api_password', 'folio', 'position',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'event_user', 'team_id', 'user_id')
            ->withPivot('start', 'end', 'user_id', 'event_id');
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
}
