<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    use HasFactory;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'effort_id', 'stat_type_id', 'value',
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\StatType', 'stat_type_id');
    }

    public function effort()
    {
        return $this->belongsTo('App\Models\Effort');
    }
}
