<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'url', 'img', 'active', 'order'];
    public $timestamps = false;

    public function base_path()
    {
        return 'sliders/' . $this->id;
    }

    public function img_path($full = true)
    {
        if ($full) {
            return url('storage/' . $this->base_path() . '/' . $this->img);
        } else {
            return '/public/' . $this->base_path() . '/' . $this->img;
        }
    }

    public function pretty_status()
    {
        return ($this->active == 1) ? '<i style="font-size:20px; color:green" class="zmdi zmdi-check zmdi-hc-fw"></i>' : '<i style="font-size:20px; color:red" class="zmdi zmdi-close zmdi-hc-fw"></i>';
    }

    public function api_img()
    {
        return 'storage/sliders/' . $this->id . '/' . $this->img;
    }

    public static function activeSliders()
    {
        return Slider::where('active', 1)->get();
    }

    public static function slidersForApp()
    {
        $sliders = array();
        foreach (Slider::activeSliders() as $slider) {
            // $img = file_get_contents($slider->img_path());

            $sliders[] = [
                'title' => $slider->title,
                // 'image' => base64_encode($img),
                'image' => asset($slider->img_path()),
                'url' => $slider->url
            ];
        }

        return $sliders;
    }
}
