<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	use HasFactory;
    protected $fillable = ['key', 'content'];
    public $timestamps = false;

    public function location(){
    	return $this->hasMany('App\Models\Location');
    }
}
