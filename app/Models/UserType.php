<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;

    protected $fillable = [
        'parent_type',
        'name',
        'active',
    ];

    public function user()
    {
        return $this->hasMany('App\Models\User');
    }

    public function pretty_name()
    {
        switch ($this->name) {
            case 'admin':
                return 'Administrador';
                break;
            case 'user':
                return 'Usuario app';
                break;
            default:
                return 'no definido';
                break;
        }
    }
}
