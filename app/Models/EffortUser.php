<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EffortUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id', 'user_id', 'start', 'end', 'date', 'folio', 'position', 'effort_id'
    ];

    public $timestamps = false;

    public function stats()
    {
        return $this->hasMany('App\Models\Stat');
    }
}
