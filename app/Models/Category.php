<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'cod_category', 'FOLini', 'FOLfin'
    ];
    public $timestamps = false;

    public function pretty_cateogri()
    {
        return $this->type == 'female' ? 'femenino' : 'masculino';
    }
}
