<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

// Encrypter class
class Encrypter extends Model
{
    // Fillable
    protected $fillable = [
        'message',
        'encryption_key',
        'initialization_vector'
    ];

    // Model attributes
    public $message;
    public $encryption_key;
    public $initialization_vector;
    public $encrypted_message;

    // Construct method
    function __construct()
    {
        // Define encryption attribute
        $this->setEncryptionKeyAttribute(env('ENCRYPTION_KEY'));

        // Define initialization vector
        $this->setInitializationVectorAttribute(env('ENCRYPTION_IV'));
    }

    // Set a message and encrypt it!
    public function setMessage($message)
    {
        // Ddefining message and encrypted message
        $this->attributes['message'] = $this->message = $message;
        $this->attributes['encrypted_message'] = $this->encrypted_message =
            $this->mod_string(base64_encode($this->encryption_key)) .
            base64_encode($this->message)  .
            $this->mod_string(base64_encode($this->initialization_vector));

        // Retrning message
        return $this->encrypted_message;
    }

    // Function to decrypt a message
    public function decryptMessage($cipher_text)
    {
        // Defining encrypted message
        $this->attributes['encrypted_message'] = $this->encrypted_message = $cipher_text;
        $this->attributes['message'] = $this->message = base64_decode(substr($cipher_text, 60, -32));

        // Returning decrypted message
        return $this->message;
    }

    // Setter for encryption key
    public function setEncryptionKeyAttribute($value)
    {
        // Defining encryption key
        $this->attributes['encryption_key'] = $this->encryption_key = $value;
    }

    // Setter for encryption iv
    public function setInitializationVectorAttribute($value)
    {
        // Defining initialization vector
        $this->attributes['initialization_vector'] = $this->initialization_vector = $value;
    }

    // Modify string
    private function mod_string($string)
    {
        /*
            Modify string, that is:
            3 random characters + the original string except the first 3 and last 5 + 5 random characters
            And that folks, is not an encrytion method, but hey...
        */
        return $this->rand_string(3) . substr($string, 3, strlen($string) - 8) . $this->rand_string(5);
    }

    // Random string
    private function rand_string($length)
    {
        // Generate a random string with a defined length
        return substr(base64_encode(random_bytes(10)), 0, $length);
    }
}
