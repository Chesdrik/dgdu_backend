<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EffortGpsLocation extends Model
{
    use HasFactory;

    protected $table = 'effort_gps_locations';
    public $timestamps = false;
    protected $fillable = [
        'id', 'effort_id', 'route'
    ];

    public function effort(){
        return $this->belongsTo('App\Effort');
    }
}
