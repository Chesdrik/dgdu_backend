<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EffortType extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function user()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public static function effort_types_array()
    {
        return EffortType::orderBy('name')->pluck('name', 'id')->toArray();
    }
}
