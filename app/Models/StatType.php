<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'unit',
    ];

    public function stat(){
    	return $this->hasMany('App\Models\Stat');
    }
}
