<?php
namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Input
        Form::macro('MDtext', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                        <div class='form-group'>
                            <label>{$label}</label>
                            <input type='text'
                                    class='form-control ".($errors->first($name) ? 'is-invalid' : '' )."'
                                    placeholder='{$label}'
                                    value='{$value}'
                                    name='{$name}'
                                    ".($options['disabled'] ? 'disabled' : '')."
                                    ".($options['required'] ? 'required' : '')."
                                    >
                            <i class='form-group__bar'></i>
                            ".($errors->first($name) ? "<div class='invalid-feedback'>{$errors->first($name)}</div>" : "")."
                        </div>
                    </div>";
        });

        // $input = '<div class="'.$options['class'].'">
        //             <div class="form-group">
        //                 <label>'.$label.'</label>
        //                 <input type="text" class="form-control '.($errors->first($name) ? 'is-invalid' : '').'"
        //                     value="'.$value.'"
        //                     name="'.$name.'"
        //                     placeholder="'.$options['placeholder'].'"
        //                     '.($options["disabled"] ? 'disabled' : '').'>';
        //                     // '.($options["disabled"] ? 'disabled', '').';
        //
        //                 // Displaying errors
        //                 if($errors->first($name)){
        //                     $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
        //                 }
        //
        //     $input .= '<i class="form-group__bar"></i>
        //             </div>
        //         </div>';
        // return $input;

        Form::macro('MDtextarea', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                        <div class='form-group'>
                            <label>{$label}</label>
                            <textarea
                                    class='form-control ".($errors->first($name) ? 'is-invalid' : '' )."'
                                    placeholder='{$label}'
                                    value='{$value}'
                                    name='{$name}'
                                    id='{$name}'
                                    ".($options['disabled'] ? 'disabled' : '')."
                                    >{$value}</textarea>
                            <i class='form-group__bar'></i>
                            ".($errors->first($name) ? "<div class='invalid-feedback'>{$errors->first($name)}</div>" : "")."
                        </div>
                    </div>";
        });

        Form::macro('MDdatepicker', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return Form::MDDatetimepickerMacro($label, $name, $value, 'YYYY-MM-DD', $errors, $options);
        });

        Form::macro('MDtimepicker', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return Form::MDDatetimepickerMacro($label, $name, $value, 'HH:mm', $errors, $options);
        });

         Form::macro('MDdatetimepicker', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return Form::MDDatetimepickerMacro($label, $name, $value, 'YYYY-MM-DD HH:mm', $errors, $options);
        });

        Form::macro('MDDatetimepickerMacro', function ($label, $name, $value, $format, $errors, $options) {
            return "<div class='{$options['class']}'>
                        <label>{$label}</label>
                        <div class='input-group form-group'>
                            <div class='input-group-prepend'>
                                <span class='input-group-text'><i class='zmdi zmdi-calendar zmdi-hc-fw'></i></span>
                            </div>
                            <input type='text'
                                    class='form-control datetimepicker-input ".($errors->first($name) ? 'is-invalid' : '' )."'
                                    data-toggle='datetimepicker'
                                    data-target='#{$name}'
                                    data-date-format='{$format}'
                                    placeholder='{$label}'
                                    value='{$value}'
                                    name='{$name}'
                                    id='{$name}'>
                            <i class='form-group__bar'></i>
                            ".($errors->first($name) ? "<div class='invalid-feedback'>{$errors->first($name)}</div>" : "")."
                        </div>
                    </div>";
        });



        // Email
        Form::macro('MDemail', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false];
            $options = array_merge($options, $c_options);

            $input = '<div class="'.$options['class'].'">
                        <div class="form-group">
                            <label>'.$label.'</label>
                            <input type="email" class="form-control '.($errors->first($name) ? 'is-invalid' : '').'"
                                value="'.$value.'"
                                name="'.$name.'"
                                placeholder="'.$options['placeholder'].'"
                                '.($options["disabled"] ? 'disabled' : '').'>';
                                // '.($options["disabled"] ? 'disabled', '').';

                            // Displaying errors
                            if($errors->first($name)){
                                $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
                            }

                $input .= '<i class="form-group__bar"></i>
                        </div>
                    </div>';
            return $input;
        });

        // Password
        Form::macro('MDpassword', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false];
            $options = array_merge($options, $c_options);

            $input = '<div class="'.$options['class'].'">
                        <div class="form-group">
                            <label>'.$label.'</label>
                            <input type="password" class="form-control '.($errors->first($name) ? 'is-invalid' : '').'"
                                value="'.$value.'"
                                name="'.$name.'"
                                placeholder="'.$options['placeholder'].'"
                                '.($options["disabled"] ? 'disabled' : '').'>';
                                // '.($options["disabled"] ? 'disabled', '').';

                            // Displaying errors
                            if($errors->first($name)){
                                $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
                            }

                $input .= '<i class="form-group__bar"></i>
                        </div>
                    </div>';
            return $input;
        });


        // Select
        Form::macro('MDselect', function ($label, $name, $value, $elements, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);


            $input = '<div class="'.$options['class'].'">
                        <div class="form-group">
                            <label>'.$label.'</label>

                            <div class="select">
                                <select class="form-control" name="'.$name.'" '.($options["disabled"] ? 'disabled' : '').'>';
                                    foreach($elements as $element => $label){
                                        $input .= '<option value="'.$element.'" '.($element == $value ? 'selected="selected"' : '').'>
                                                        '.$label.'
                                                    </option>';
                                    }
                            $input .= '
                                </select>
                                <i class="form-group__bar"></i>
                            </div>';

                            // Displaying errors
                            if($errors->first($name)){
                                $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
                            }

                $input .= '
                        </div>
                    </div>';

            return $input;
        });


        Form::macro('MDfile', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);

            // Displaying errors
            // if($errors->first($name)){
            //     $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
            // }


            return "<div class='{$options['class']}'>
                        <div class='input-group'>
                            <label for='{$name}'>{$label}</label>
                            <input type='file' class='form-control-file' id='{$name}' name='{$name}' ".($options["disabled"] ? 'disabled' : '').">
                        </div>
                    </div>";
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
