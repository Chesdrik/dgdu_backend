<?php
namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class ButtonMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Submit button
        Form::macro('MDsubmit', function ($label, $id = '', $c_options = []) {
            $options = ['class' => 'btn btn-light btn--icon-text btn-puma-off pull-right', 'icon' => 'check'];
            $options = array_merge($options, $c_options);

            return '<div class="col-md-12">
                        <button class="'.$options['class'].'" id="'.$id.'" class="btn btn-light btn--icon-text btn-puma-off pull-right">
                            <i class="zmdi zmdi-'.$options['icon'].'"></i> '.$label.'
                        </button>
                    </div>';


            $btn = '<button type="submit" class="'.$class.'" id="'.$id.'">'.$label.'</button>';
            return $btn;
        });

        // Submit button
        Form::macro('MDdelete', function ($label, $id = '', $c_options = []) {
            $options = ['class' => 'btn btn-danger btn--icon-text btn-block text-center btn-lg pull-right', 'icon' => 'check'];
            $options = array_merge($options, $c_options);

            return '<div class="col-md-12">
                        <button class="'.$options['class'].'" id="'.$id.'" class="btn btn-light btn--icon-text btn-puma-off pull-right">
                            <i class="zmdi zmdi-'.$options['icon'].'"></i> '.$label.'
                        </button>
                    </div>';


            $btn = '<button type="submit" class="'.$class.'" id="'.$id.'">'.$label.'</button>';
            return $btn;
        });


        // Form::macro('MDButton', function($label, $id = '', $url, $c_options = []){
        //     $options = ['class' => 'btn btn-light btn--raised'];
        //     $options = array_merge($options, $c_options);

        //     return '<a href="'.url($url).'" target="_self">
        //                 <button type="button" id="'.$id.'" class="'.$options['class'].'">
        //                     '.$label.'
        //                 </button>
        //             </a>';
        // });

        Form::macro('MDButtonSeatType', function($label, $id, $color, $c_options = []){
            $options = ['class' => 'btn btn-light btn--raised'];
            $options = array_merge($options, $c_options);

            return '<button type="button" class="'.$options['class'].'" seat-type="seat_'.$id.'">
                        <span class="dot m-r-15" style="background-color: #'.$color.'"></span> <span class="seat-type-label">'.$label.'</span>
                    </button>';
        });

        Form::macro('MDButtonDeleteOnCard', function(){
            return '<a href="#" class="actions__item zmdi zmdi-delete" onclick="$(this).parent().submit();"></a>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
