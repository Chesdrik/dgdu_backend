<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu password ha sido actualizado!',
    // 'sent' => 'Te enviamos un link a tu correo para cambiar el password',
    'sent' => "Se ha enviado un mensaje de recuperación al correo electrónico ingresado",
    'throttled' => 'Por favor espera antes de reintentar.',
    'token' => 'El token es inválido.',
    // 'user' => "Usuario inexistente o incorrecto.",
    'user' => "Se ha enviado un mensaje de recuperación al correo electrónico ingresado",

];
