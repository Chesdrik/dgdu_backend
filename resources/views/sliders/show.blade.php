@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Editar {{ $slider->title }}</h1>
            </header>
        </div>
    </div>

    @include('includes.success')
    @include('includes.error')

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('sliders.update', $slider->id), 'method' => 'put',  'enctype' => "multipart/form-data")) !!}
                        <div class="row">
                            {!! Form::MDtext('Título', 'title', $slider->title, $errors, ['class' => 'col-md-12', 'required' => true]) !!}
                            {!! Form::MDtext('URL', 'url', $slider->url, $errors, ['class' => 'col-md-12', 'required' => false]) !!}

                            {!! Form::MDselect('Activo', 'active', $slider->active, [1 => 'Sí', 0 => 'No'], $errors, ['class' => 'col-md-6', 'required' => true]) !!}
                            {!! Form::MDtext('Orden', 'order', $slider->order, $errors, ['class' => 'col-md-6', 'required' => true]) !!}

                            {!! Form::MDfile('Imagen', 'img', '', $errors) !!}

                            @if($slider->img)
                                <div class="col-sm-12 m-b-15">
                                    <img src="{{ $slider->img_path() }}" class="img-fluid w-100" />
                                </div>
                            @else
                                <div class="col-sm-12 text-center" style="opacity: 0.5">
                                    <br />
                                    NO HAY IMAGEN
                                </div>
                            @endif
                            <br><br><br>


                            {!! Form::MDsubmit('Editar', 'create', ['icon' => 'plus-save', 'class' => 'btn btn-success btn--icon-text pull-right btn-block text-center btn-lg pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row m-b-25">
        <div class="col-sm-8 offset-sm-2">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('sliders.destroy', $slider->id), 'method' => 'DELETE')) !!}
                        {!! Form::MDdelete('Eliminar', 'create', ['icon' => 'plus-save']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
