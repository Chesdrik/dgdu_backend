@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Sliders</h1>
        <div class="actions">
            <a href="" data-toggle="modal" data-target="#create-slider" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Crear slider
            </a>
            @include('sliders.modals.create')
        </div>
    </header>

    @include('includes.success')
    @include('includes.error')

    @if(count($sliders) > 0)
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Sliders</h4>
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th>&nbsp;</th>
                            <th>Título</th>
                            <th>URL</th>
                            <th>Activo</th>
                            <th>Orden</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sliders as $slider)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('sliders.show', $slider->id) }} ">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $slider->title }}</td>
                                <td>{{ $slider->url }}</td>
                                <td>{!! $slider->pretty_status() !!}</td>
                                <td>{{ $slider->order }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <br>
            NO HAY SLIDERS
    </div>
    @endif

@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable()
    });

</script>
@endsection
