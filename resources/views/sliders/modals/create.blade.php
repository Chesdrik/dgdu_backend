<div class="modal fade" id="create-slider" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el slider.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Crear slider</h5>
                <hr>
            </div>

            <div class="modal-body">
                {!! Form::open(array('url' => route('sliders.store'), 'method' => 'POST',  'enctype' => "multipart/form-data")) !!}
                    {!! Form::MDtext('Título', 'title', old('title'), $errors, ['class' => 'col-md-12']) !!}
                    {!! Form::MDtext('URL', 'url', old('url'), $errors, ['class' => 'col-md-12', 'required' => false]) !!}
                    {!! Form::MDselect('Activo', 'active', old('active'), [1 => 'Sí', 0 => 'No'], $errors, ['class' => 'col-md-12']) !!}
                    {!! Form::MDtext('Orden', 'order', old('order'), $errors, ['class' => 'col-md-12']) !!}
                    {!! Form::MDfile('Imagen', 'img', '', $errors) !!}

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
