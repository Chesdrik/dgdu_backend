<div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-md-6">Nombre</th>
                            <th class="@if($this->team) col-md-3 @else col-md-3 @endif">Folio</th>
                            @if($this->team)
                                <th class="col-md-3">Equipo</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    {{ $user->full_name }}
                                    <small>{{ $user->CURP }}</small>
                                </td>
                                <td>{{ $user->pivot->folio }}</td>
                                @if($this->team)
                                    <td>{{ $this->teams[$user->pivot->team_id] }}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="pagination justify-content-center m-t-15">
                    {!! $users->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
