<div>
    <div class="col-sm-10 offset-1">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Registrar evento @if(!is_null($this->eventType)) tipo {{ $this->eventType->name }} @endif</h4>

                <form id="block-form" wire:submit.prevent="submit" >
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control " placeholder="Nombre" value="" name="name" wire:model.defer="name">
                                @error('name') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                                <i class="form-group__bar"></i>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>NOID</label>
                                <input type="text" class="form-control " placeholder="NOID" value="0" name="noid" wire:model.defer="noid">
                                @error('noid') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                                <i class="form-group__bar"></i>
                            </div>
                        </div>

                        <div class="@if(is_null($this->eventType)) col-sm-12 @elseif($this->eventType->team == 1) col-sm-4 @else col-sm-6 @endif">
                            <div class="form-group">
                                <label for="eventTypeId" class="col-form-label">Tipo de evento</label>
                                <div class="form-group">
                                    <select class="form-control" wire:model="eventTypeId">
                                        <option value="0">Selecciona un tipo de evento</option>
                                        @foreach($eventTypesArray as $id => $eventTypeName)
                                            <option value="{{ $id }}">{{ $eventTypeName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @error('eventTypeId') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                            </div>
                        </div>

                        @if(!is_null($this->eventType))
                            <div class="@if($this->eventType->team == 1) col-sm-4 @else col-sm-6 @endif">
                                <div class="form-group">
                                    <label for="effortTypeId" class="col-form-label">Tipo de esfuerzo</label>
                                    <div class="form-group">
                                        <select class="form-control" wire:model="effortTypeId">
                                            <option value="0">Selecciona un tipo de esfuerzo</option>
                                            @foreach($effortTypesArray as $id => $effortType)
                                                <option value="{{ $id }}">{{ $effortType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('effortTypeId') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                                </div>
                            </div>

                            @if($this->eventType->team == 1)
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="max_users" class="col-form-label">Máximo de registros por equipo</label>
                                        <input type="number" class="form-control checkout-form @error('max_users') is-invalid @enderror" placeholder="" wire:model="max_users">
                                        @error('max_users') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            @endif
                        @endif

                        <div class="col-md-6">
                            <label>Fecha inicio</label>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i></span>
                                </div>
                                <input type="text" class="form-control flatpickr-input-start" value="" name="start" id="start" wire:model="start">
                                @error('start') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                                <i class="form-group__bar"></i>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <label>Fecha fin</label>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="zmdi zmdi-calendar zmdi-hc-fw"></i></span>
                                </div>
                                <input type="text" class="form-control flatpickr-input-end" value="" name="end" id="end" wire:model="end">
                                @error('end') <div class="invalid-feedback" style="display: block;">{{ $message }}</div> @enderror
                                <i class="form-group__bar"></i>
                            </div>
                        </div>

                        {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
