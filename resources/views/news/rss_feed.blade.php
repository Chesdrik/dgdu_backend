@extends('layouts.app')
@section('content')
    <header class="content__title">
    </header>

    @include('includes.success')
    @include('includes.error')

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-4">Titulo</th>
                            <th class="col-sm-5">Cotenido</th>
                            <th class="col-sm-2">Link</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rss_feed->get_items() as $rss_news)
                            <tr>
                                <td class="text-center">
                                    <a href="#">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-plus"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $rss_news->get_title() }}</td>
                                <td>{{ $rss_news->get_content() }}</td>
                                <td>{{ $rss_news->get_link() }}</td>
                                <td>{{ $rss_news->get_date() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable()
    });

</script>
@endsection
