@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Noticias</h1>
        <div class="actions">
            <a href="{{ route('news.rss_fetch') }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar con RSS
            </a>
            <a href="{{ route('news.create') }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar noticia
            </a>
        </div>
    </header>

    @include('includes.success')
    @include('includes.error')

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-6">Titulo</th>
                            <th class="col-sm-5">Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($news as $new)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('news.show',  $new->id) }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $new->title }}</td>
                                <td>{{ $new->date }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable()
    });

</script>
@endsection
