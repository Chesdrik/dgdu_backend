@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Registrar Noticia</h1>
            </header>
        </div>
    </div>

    @include('includes.success')
    @include('includes.error')


    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('news.store'), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                        {!! Form::MDtext('Titulo', 'title',  old('title'), $errors) !!}
                        {!! Form::MDdatetimepicker('Fecha', 'date', old('date'), $errors) !!}
                        {!! Form::MDtextarea('Contenido', 'content', old('content'), $errors) !!}
                        {!! Form::MDfile('Imagen (png)', 'img', '', $errors) !!}
                        {!! Form::MDselect('Activo', 'active', '1', ['1' => 'Si', '0'=>'No'], $errors) !!}
                        <div id="contenido"></div>
                        {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after_includes')

<script>




  ClassicEditor.create( document.querySelector( '#content' ) )
        .catch( error => {
            console.error( error );
        } );


</script>
@endsection
