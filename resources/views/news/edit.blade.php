@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Registrar Noticia</h1>
            </header>
        </div>
    </div>

    @include('includes.success')
    @include('includes.error')

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('news.update', $news->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data')) !!}
                        {!! Form::MDtext('Titulo', 'title', $news->title, $errors) !!}
                        {!! Form::MDdatetimepicker('Fecha', 'date', $news->date, $errors) !!}
                        {!! Form::MDtextarea('Contenido', 'content', $news->content, $errors) !!}

                        {!! Form::MDfile('Imagen (png)', 'img', '', $errors) !!}
                        <div class="col-sm-12 m-b-15">
                            <img src="{{ Asset($news->api_img()) }}" class="img-fluid w-100" />
                        </div>

                        {!! Form::MDselect('Activo', 'active', $news->active, ['1' => 'Si', '0'=>'No'], $errors) !!}
                        <div id="contenido"></div>
                        {!! Form::MDsubmit('Editar', 'create', ['icon' => 'plus-save', 'class' => 'btn btn-success btn--icon-text pull-right btn-block text-center btn-lg pull-right']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row m-b-25">
        <div class="col-sm-8 offset-sm-2">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('news.destroy', $news->id), 'method' => 'DELETE')) !!}
                        {!! Form::MDdelete('Eliminar', 'create', ['icon' => 'plus-save']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_includes')
<script>
    ClassicEditor.create( document.querySelector( '#content' ))
        .catch( error => {
            console.error( error );
        }
    );
</script>
@endsection
