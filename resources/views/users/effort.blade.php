@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Dashboard</h1>
    </header>

    @include('includes.success')
    @include('includes.error')

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-2">Tipo</th>
                            <th class="col-sm-3">Fecha</th>
                            <th class="col-sm-2">Tiempo</th>
                            <th class="col-sm-4">Estadísticas</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($user->effort()->orderBy('created_at', 'DESC')->get() as $effort)

                            <tr>
                                <td class="text-center">
                                    @if($effort->effort_gps_locations()->exists())
                                        <div class="actions">
                                            <a href="{{ route('kml_download', $effort->id) }}" class="btn btn-outline-secondary zmdi zmdi-map"></a>
                                        </div>
                                    @else
                                        &nbsp;
                                    @endif
                                </td>
                                <td>{{ $effort->effort_type->name }}</td>
                                <td>{{ $effort->pretty_timestamps() }}</td>
                                <td>{{ $effort->effort_time_in_minutes() }} min</td>
                                <td>
                                    @foreach($effort->stats as $stat)
                                        <strong>{{ $stat->type->name }}:</strong> {{ $stat->value }}<br />
                                    @endforeach
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
@endsection

@section('after_includes')

<script type="text/javascript">
    $(document).ready(function() {
        $(".data-table").DataTable().order([2, 'desc']).draw();
    });
</script>

@endsection
