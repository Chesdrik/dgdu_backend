@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Usuarios {{ $user_type }}</h1>
        <div class="actions">
            <a href="{{ route('users.index.admins') }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-accounts"></i> Administradores
            </a>
            <a href="{{ route('users.index.appusers') }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-accounts"></i> Usuarios App
            </a>
            <a href="" data-toggle="modal" data-target="#crear-user" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar usuario
            </a>
        </div>
    </header>

    @include('includes.success')
    @include('includes.error')

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-2">&nbsp;</th>
                            <th class="col-sm-4">Nombre</th>
                            <th class="col-sm-4">E-mail</th>
                            <th class="col-sm-2">Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">
                                    <a href="" data-toggle="modal" data-target="#editar-{{ $user->id }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                    @if($user_type != 'administradores')
                                        <a href="{{ route('users.effort', $user->id) }}">
                                            <button class="btn btn-outline-secondary btn--raised">
                                                <i class="zmdi zmdi-eye"></i>
                                            </button>
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->type->pretty_name() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- Modal create --}}
    @if($user_type == 'administradores')
        @include('users.modals.create_admin')
    @else
        @include('users.modals.create_user')
    @endif

    {{-- Edit user modals --}}
    @foreach ($users as $user)
        @include('users.modals.edit')
    @endforeach
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable()
    });

</script>
@endsection
