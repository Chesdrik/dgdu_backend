<div class="modal fade" id="crear-user" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el usuario.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar usuario</h5>
                <hr>
            </div>

            <div class="modal-body">
                <form method="POST" action="{{ route('users.store') }}" class="row">
                    @csrf

                    {!! Form::hidden('user_type_id', 1) !!}
                    {!! Form::MDtext('Nombre', 'name', old('name'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDtext('Primer Apellido', 'last_name', old('last_name'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDtext('Segundo Apellido', 'second_last_name', old('second_last_name'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDemail('Email', 'email', old('email'), $errors, ['class' => 'col-md-6']) !!}
                    {{-- <div class="col-md-6"></div> --}}
                    {!! Form::MDpassword("Password (<small>{$random_pswd}</small>)", 'password', $random_pswd, $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDpassword('Confirmar Password', 'password_confirmation', $random_pswd, $errors, ['class' => 'col-md-6']) !!}

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
