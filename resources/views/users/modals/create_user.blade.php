<div class="modal fade" id="crear-user" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el usuario.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar usuario</h5>
                <hr>
            </div>

            <div class="modal-body">
                <form method="POST" action="{{ route('users.store') }}" class="row">
                    @csrf
                    {!! Form::MDtext('Nombre', 'name', old('name'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDtext('Primer Apellido', 'last_name', old('last_name'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDtext('Segundo Apellido', 'second_last_name', old('second_last_name'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDdatepicker('Fecha de nacimiento', 'birthdate', old('birthdate'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDselect('Tipo usuario', 'user_type_id', old('user_type_id'), $user_types, $errors, ['class' => 'col-md-12']) !!}
                    {!! Form::MDtext('CURP', 'CURP', old('CURP'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDselect('Sexo', 'gender', 'male', ['male' => 'Hombre', 'famale' => 'Mujer'], $errors,['class' => 'col-md-6'] ) !!}
                    {!! Form::MDemail('Email', 'email', old('email'), $errors, ['class' => 'col-md-6']) !!}
                    <div class="col-md-6"></div>
                    {!! Form::MDpassword('Password', 'password', old('password'), $errors, ['class' => 'col-md-6']) !!}
                    {!! Form::MDpassword('Confirmar Password', 'password_confirmation', old('password_confirmation'), $errors, ['class' => 'col-md-6']) !!}



                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
