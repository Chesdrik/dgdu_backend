<div class="modal fade" id="editar-{{ $user->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo acutlizar la información del usuario.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Editar {{ $user->name }}</h5>
                <hr>
            </div>

            <div class="modal-body">
                {!! Form::open(array('url' => route('users.update', $user->id), 'method' => 'put', 'class' => 'row')) !!}

                    {!! Form::MDtext('Nombre', 'name', $user->name, $errors, ['class' => 'col-md-6', 'required' => true]) !!}
                    {!! Form::MDtext('Primer Apellido', 'last_name', $user->last_name, $errors, ['class' => 'col-md-6', 'required' => true]) !!}
                    {!! Form::MDtext('Segundo Apellido', 'second_last_name', $user->second_last_name, $errors, ['class' => 'col-md-6', 'required' => true]) !!}
                    @if($user_type != 'administradores')
                        {!! Form::MDdatepicker('Fecha de nacimiento', 'birthdate', $user->birthdate, $errors, ['class' => 'col-md-6', 'required' => true]) !!}
                        {!! Form::MDtext('CURP', 'CURP', $user->CURP, $errors, ['class' => 'col-md-6', 'required' => true]) !!}
                        {!! Form::MDselect('Sexo', 'gender', $user->gender, ['male' => 'Hombre', 'famale' => 'Mujer'], $errors,['class' => 'col-md-6'] ) !!}
                    @endif
                    {!! Form::MDemail('Email', 'email', $user->email, $errors, ['class' => 'col-md-6', 'required' => true]) !!}




                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
