@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Dashboard</h1>
    </header>

    <div class="card">
        <div class="card-body">
            <div class="row p-t-35">
                <a href="{{ route('events.index') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Eventos
                    </div>
                </a>

                <a href="{{ route('news.index') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Noticias
                    </div>
                </a>

                <a href="{{ route('sliders.index') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Sliders
                    </div>
                </a>

                <a href="{{ route('users.index') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Estadísticas
                    </div>
                </a>

                <a href="{{ route('users.index.admins') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Administradores
                    </div>
                </a>
                <a href="{{ route('users.index.appusers') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Usuarios App
                    </div>
                </a>
                <a href="{{ route('content.index') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Textos
                    </div>
                </a>
                <a href="{{ route('video.index') }}" class="col-sm-4 m-b-50">
                    <div class="btn btn-block btn-lg btn-outline-puma btn-puma p-t-35 p-b-35 shadow">
                        Videos
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection
