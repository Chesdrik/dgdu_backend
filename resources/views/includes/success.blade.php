@if(session('success'))
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <div class="col-sm-12">
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        </div>
    </div>
</div>
@endif
