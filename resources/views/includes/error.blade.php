@if ($errors->any())
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-danger" role="alert">
                    Se encontraron errores con los datos, por favor revisa la información ingresada:
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
