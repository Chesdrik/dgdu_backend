@extends('layouts.app_clean')

@section('content')

    <div class="login">



        <!-- Login -->
        <div class="login__block active" id="l-login" style="background-color: white;">

            @if(session('error'))
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                </div>
            </div>
            @endif

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <img src="{{ asset('/images/logo.png') }}" style="height: 100px;" />

                <div class="login__block__body">
                    <div class="form-group form-group--float form-group--centered">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        <label>Email</label>
                        <i class="form-group__bar"></i>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group form-group--float form-group--centered">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        <label>Password</label>
                        <i class="form-group__bar"></i>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" href="{{ url('/admin') }}" class="btn btn--icon login__block__btn bluegray-light-bg"><i class="zmdi zmdi-long-arrow-right"></i></button>
                </div>
            </form>
            {{-- <a href="{{ route('register') }}">Registrarse</a> --}}
             <a class="btn btn-link" href="{{ route('password.request') }}">
                ¿Olvidaste tu contraseña?
            </a>
        </div>


        <!-- Forgot Password -->
        <div class="login__block" id="l-forget-password">
            <div class="login__block__header palette-Purple bg">
                <i class="zmdi zmdi-account-circle"></i>
                Forgot Password?

                <div class="actions actions--inverse login__block__actions">
                    <div class="dropdown">
                        <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">Already have an account?</a>
                            <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-register" href="">Create an account</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="login__block__body">
                <p class="mt-4">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>

                <div class="form-group form-group--float form-group--centered">
                    <input type="text" class="form-control">
                    <label>Email Address</label>
                    <i class="form-group__bar"></i>
                </div>

                <button href="index.html" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-check"></i></button>
            </div>
        </div>
    </div>

    <!-- Older IE warning message -->
        <!--[if IE]>
            <div class="ie-warning">
                <h1>Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade to any of the following web browsers to access this website.</p>

                <div class="ie-warning__downloads">
                    <a href="http://www.google.com/chrome">
                        <img src="img/browsers/chrome.png" alt="">
                    </a>

                    <a href="https://www.mozilla.org/en-US/firefox/new">
                        <img src="img/browsers/firefox.png" alt="">
                    </a>

                    <a href="http://www.opera.com">
                        <img src="img/browsers/opera.png" alt="">
                    </a>

                    <a href="https://support.apple.com/downloads/safari">
                        <img src="img/browsers/safari.png" alt="">
                    </a>

                    <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">
                        <img src="img/browsers/edge.png" alt="">
                    </a>

                    <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                        <img src="img/browsers/ie.png" alt="">
                    </a>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>
        <![endif]-->
@endsection
