@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Editar playlist</h1>
            </header>
        </div>
    </div>

    @include('includes.success')
    @include('includes.error')

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('video.update', $video->id), 'method' => 'PUT')) !!}
                        {!! Form::MDtext('Titulo', 'title',  $video->title, $errors) !!}
                        {!! Form::MDtext('Play list', 'playlist_id',  $video->playlist_id , $errors) !!}
                        {!! Form::MDtext('Orden', 'order',  $video->order, $errors) !!}
                        {!! Form::MDsubmit('Editar', 'create', ['icon' => 'plus-save', 'class' => 'btn btn-success btn--icon-text pull-right btn-block text-center btn-lg pull-right']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row m-b-25">
        <div class="col-sm-8 offset-sm-2">
            <hr />
        </div>
    </div>

@endsection

@section('after_includes')
<script>
    ClassicEditor.create( document.querySelector( '#content' ))
        .catch( error => {
            console.error( error );
        }
    );
</script>
@endsection
