@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>videos</h1>
        <div class="actions">
            
            <a href="{{ route('video.create') }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Agrear playlist
            </a>
        </div>
    </header>

    @include('includes.success')
    @include('includes.error')

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-1">orden</th>
                            <th class="col-sm-6">Titulo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($videos as $video)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('video.show',  $video->id) }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                    
                                </td>
                                <td>
                                    {!! Form::open(array('url' => route('video.destroy', $video->id), 'method' => 'DELETE')) !!}
                                        {!! Form::MDdelete('', 'delete', ['icon' => 'zmdi zmdi-delete zmdi-hc-fw', 'class' =>'btn btn-outline-secondary btn--raised']) !!}
                                    {!! Form::close() !!}
                                </td>
                                <td>{{$video->order}}</td>
                                <td>{{ $video->title }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable()
    });

</script>
@endsection
