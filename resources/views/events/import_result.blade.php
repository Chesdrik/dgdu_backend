@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>importar archivo</h1>
            </header>
        </div>
    </div>

    @include('includes.success')
    @include('includes.error')


    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                     {!! Form::open(array('url' => route('event.import', $event_id), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                        @foreach($categories as $categori)
                            {!! Form::MDfile('Resultados de '.$categori->name, $categori->id, '', $errors) !!}

                        @endforeach
                       
                        {!! Form::MDsubmit('Subir archivo', 'upload', ['icon' => ' zmdi-upload']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after_includes')

<script>




  ClassicEditor.create( document.querySelector( '#content' ) )
        .catch( error => {
            console.error( error );
        } );


</script>
@endsection
