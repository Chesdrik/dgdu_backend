@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Crear evento</h1>
    </header>

    <livewire:events.create :eventTypesArray="$event_types_array" :effortTypesArray="$effort_types_array" />
@endsection


@section('after_includes')
    <script type="text/javascript">
        // optional_config
        $(document).ready(function(){
            $(".flatpickr-input-start").flatpickr({
                enableTime: true,
                dateFormat: "Y-m-d H:i:00",
                minDate: "{{ \Carbon\Carbon::now() }}"
            });

            $(".flatpickr-input-end").flatpickr({
                enableTime: true,
                dateFormat: "Y-m-d H:i:00",
                minDate: "{{ \Carbon\Carbon::now() }}"
            });
        });
    </script>
@endsection
