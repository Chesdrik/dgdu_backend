@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Eventos</h1>
        <div class="actions">
            <a href="{{ route('events.create')}}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar evento
            </a>
            {{-- @include('events.modals.create') --}}
        </div>
    </header>

    @include('includes.success')
    @include('includes.error')

    @if(count($upcoming_events) > 0)
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Eventos</h4>
            <h6 class="card-subtitle">Listado de eventos próximos a realizarse</h6>
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-7">Nombre</th>
                            <th class="col-sm-4">Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($upcoming_events as $event)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('events.show', $event->id) }}" target="_self">
                                        {{-- data-toggle="modal" data-target="#edit-{{ $event->id }}" --}}
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $event->name }}</td>
                                <td>{{ pretty_date($event->date) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <br>
            NO HAY EVENTOS PRÓXIMOS
    </div>
    @endif

    <hr>

    @if(count($past_events) > 0)
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Eventos pasados</h4>
            <h6 class="card-subtitle">Listado de eventos pasados</h6>
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-7">Nombre</th>
                            <th class="col-sm-4">Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($past_events as $event)
                            <tr>
                                 <td class="text-center">
                                     <a href="{{ route('event.import_index', $event->id) }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-upload zmdi-hc-fw"></i>
                                        </button>
                                    </a>
                                    @include('events.modals.result_import')
                                </td>
                                <td>{{ $event->name }}</td>
                                <td>{{ pretty_date($event->date) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif

    @foreach($upcoming_events as $event)
        @include('events.modals.edit')

    @endforeach
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable()
    });
</script>
@endsection
