@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>{{ $event->name }}</h1>
    </header>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ficha de evento</h4>
                </div>

                <div class="listview listview--bordered listview--hover">
                    <div class="listview__item">
                        <div class="listview__content">
                            <div class="listview__heading">Nombre</div>
                            <p>{{ $event->name }}</p>
                        </div>
                    </div>

                    <div class="listview__item">
                        <div class="listview__content">
                            <div class="listview__heading">Inicio</div>
                            <p>{{ $event->start }}</p>
                        </div>
                    </div>

                    <div class="listview__item">
                        <div class="listview__content">
                            <div class="listview__heading">Fin</div>
                            <p>{{ $event->end }}</p>
                        </div>
                    </div>

                    @if(!is_null($event->noid))
                        <div class="listview__item">
                            <div class="listview__content">
                                <div class="listview__heading">NOID</div>
                                <p>{{ $event->noid }}</p>
                            </div>
                        </div>
                    @endif

                    @if($event->event_type->team)
                        <div class="listview__item">
                            <div class="listview__content">
                                <div class="listview__heading">Máximo usuarios</div>
                                <p>{{ $event->max_users }}</p>
                            </div>
                        </div>
                    @endif

                    <div class="listview__item">
                        <div class="listview__content">
                            <div class="listview__heading">Tipo de evento</div>
                            <p>{{ $event->event_type->name }} <small>({{ $event->event_type->key }})</small></p>
                        </div>
                    </div>

                    <div class="listview__item">
                        <div class="listview__content">
                            <div class="listview__heading">Fecha creación</div>
                            <p>{{ $event->created_at }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <livewire:events.list-users :event="$event" />
        </div>
    </div>

@endsection
