<div class="modal fade" id="create-event" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el evento.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar evento</h5>
                <hr>
            </div>

            <div class="modal-body">
                <form method="POST" action="" class="row">
                    @csrf

                    {!! Form::MDtext('Nombre', 'name', '', $errors, ['class' => 'col-md-12', 'required' => true]) !!}
                    {!! Form::MDtext('NOID', 'noid', '', $errors, ['class' => 'col-md-12', 'required' => true]) !!}
                    {!! Form::MDselect('Tipo', 'type', 'CUC', ['CUC' => 'CUC', 'AFA'=>'AFA'],$errors, ['class' => 'col-md-12', 'required' => true]) !!}

                    {!! Form::MDdatepicker('Fecha', 'date', '', $errors, ['class' => 'col-md-6', 'required' => true]) !!}

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
