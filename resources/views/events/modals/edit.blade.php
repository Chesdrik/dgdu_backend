<div class="modal fade" id="edit-{{ $event->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo acutlizar la información del evento.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Editar {{ $event->name }}</h5>
                <hr>
            </div>
            
            <div class="modal-body">
                {!! Form::open(array('url' => route('events.update', $event->id), 'method' => 'put', 'class' => 'row')) !!}
                @method('PUT')

                    {!! Form::MDtext('Nombre', 'name', $event->name, $errors, ['class' => 'col-md-12', 'required' => true]) !!}
                    {!! Form::MDtext('NOID', 'noid', $event->noid, $errors, ['class' => 'col-md-12', 'required' => true]) !!}
                    {!! Form::MDselect('Tipo', 'type', 'CUC', ['CUC' => 'CUC', 'AFA'=>'AFA'],$errors, ['class' => 'col-md-12', 'required' => true]) !!}
                    {!! Form::MDdatepicker('Fecha', 'date', $event->date, $errors, ['class' => 'col-md-6', 'required' => true]) !!}

                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
