<div class="modal fade" id="import-{{ $event->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo acutlizar la información del evento.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Importar resultados del evento {{ $event->name }}</h5>
                <hr>
            </div>
            
            <div class="modal-body">
                {!! Form::open(array('url' => route('event.import', $event->id), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                      
                        {!! Form::MDfile('Resultados de alumnos', 'student', '', $errors) !!}
                        {!! Form::MDfile('Resultados de cch', 'cch', '', $errors) !!}
                        {!! Form::MDfile('Resultados de licenciatura', 'lic', '', $errors) !!}
                        {!! Form::MDfile('Resultados posgrado', 'postgraduate', '', $errors) !!}
                       
                        {!! Form::MDsubmit('Subir archivo', 'upload', ['icon' => ' zmdi-upload']) !!}
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
