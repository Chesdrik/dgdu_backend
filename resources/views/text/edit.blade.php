@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Editar Texto</h1>
            </header>
        </div>
    </div>

    @include('includes.success')
    @include('includes.error')

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('content.update', $content->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data')) !!}
                        {!! Form::MDtext('Titulo', 'title', $content->key, $errors) !!}
                        {!! Form::MDtext('Lat', 'lat', $content->location->first()->lat ?? "", $errors) !!}
                        {!! Form::MDtext('Long', 'long', $content->location->first()->long ?? "", $errors) !!}
                        {!! Form::MDtextarea('Contenido', 'content', $content->content, $errors) !!}
                        {!! Form::MDsubmit('Editar', 'create', ['icon' => 'plus-save', 'class' => 'btn btn-success btn--icon-text pull-right btn-block text-center btn-lg pull-right']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row m-b-25">
        <div class="col-sm-8 offset-sm-2">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('content.destroy', $content), 'method' => 'DELETE')) !!}
                        {!! Form::MDdelete('Eliminar', 'create', ['icon' => 'plus-save']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_includes')
<script>
    ClassicEditor.create( document.querySelector( '#content' ))
        .catch( error => {
            console.error( error );
        }
    );
</script>
@endsection
