<footer class="footer hidden-xs-down">
    <p>
        <img src="{{ Asset('/images/soy_deporte_unam_azul.png') }}" class="footer-logo" />
        <br /><br />
        {{ \Carbon\Carbon::now()->format('Y') }} © Todos los derechos reservados
    </p>
</footer>
