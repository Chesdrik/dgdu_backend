<aside class="sidebar">
    <div class="scrollbar-inner">
        <div class="m-b-15">
            <img src="{{ Asset('/images/logo_unam_azul.png') }}" class="header-logo" />
            <img src="{{ Asset('/images/logo_deporte_unam.png') }}" class="header-logo" />
        </div>
        <ul class="navigation">
            <?php
                $menu = array();
                    $menu[] = ['text' => 'Dashboard', 'url' => route('dashboard'), 'target' => '_self', 'icon' => 'home', 'name' => 'dashboard'];
                    $menu[] = ['text' => 'Eventos', 'url' => route('events.index'), 'target' => '_self', 'icon' => 'calendar', 'name' => 'events'];
                    $menu[] = ['text' => 'Noticias', 'url' => route('news.index'), 'target' => '_self', 'icon' => 'collection-text', 'name' => 'news'];
                    $menu[] = ['text' => 'Sliders', 'url' => route('sliders.index'), 'target' => '_self', 'icon' => 'collection-image', 'name' => 'sliders'];
                    $menu[] = ['text' => 'Textos', 'url' => route('content.index'), 'target' => '_self', 'icon' => 'collection-text', 'name' => 'content'];
                    $menu[] = ['text' => 'Videos', 'url' => route('video.index'), 'target' => '_self', 'icon' => 'zmdi zmdi-youtube zmdi-hc-fw', 'name' => 'video'];
                    $menu[] = ['text' => 'Estadisticas', 'url' => '', 'target' => '_self', 'icon' => 'zmdi zmdi-chart zmdi-hc-fw', 'name' => 'accesos'];
                    // $menu[] = ['text' => 'Administradores', 'url' => route('users.index'), 'target' => '_self', 'icon' => 'accounts', 'name' => 'users'];
                    $menu[] = ['text' => 'Usuarios', 'url' => route('users.index'), 'target' => '_self', 'icon' => 'accounts', 'name' => 'users'];
                    // $menu[] = ['text' => 'Login', 'url' => '/login', 'target' => '_self', 'icon' => 'sign-in', 'name' => 'login'];
            ?>
            @foreach($menu as $m)
            @php
               $class = (strpos(Route::currentRouteName(), $m["name"]) === 0) ? "navigation__active" : '';
               $class = (isset($m['submenu'])) ? $class." navigation__sub" : $class.'';
            @endphp
                <li class= {{ $class }}>
                    <a href="{{ url($m['url']) }}">
                        <i class="zmdi zmdi-{{ $m['icon'] }}"></i> {{ $m['text'] }}
                    </a>

                    @if(isset($m['submenu']))
                        <ul>
                            @foreach($m['submenu'] as $sm)
                                <li @if(strpos(Route::currentRouteName(), $sm["name"]) !== false) class="navigation__active" @endif>
                                    <a href="{{ url($sm['url']) }}" target="{{ $sm['target'] }}">- {{ $sm['text'] }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif

                </li>
            @endforeach

            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <hr />

                    <button class="btn logout-button" type="submit">
                        <i class="zmdi zmdi-lock-outline logout-icon"></i> Logout
                    </button>
                </form>
            </li>
        </ul>
    </div>
</aside>
