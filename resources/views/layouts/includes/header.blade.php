<header class="header">
    <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
        <div class="navigation-trigger__inner">
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
        </div>
    </div>

    <div class="header__logo hidden-sm-down">
        <h1><a href="{{ url('/admin') }}">Deporte<span class="oro">UNAM</span></a></h1>
    </div>


    <ul class="top-nav">

        <li class="dropdown top-nav__notifications">
            <a href="" data-toggle="dropdown" class="top-nav__notify">
                <i class="zmdi zmdi-notifications"></i>
            </a>
            <!-- <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                <div class="listview listview--hover">
                    <div class="listview__header">
                        Notificaciones

                        <div class="actions">
                            <a href="" class="actions__item zmdi zmdi-check-all" data-ma-action="notifications-clear"></a>
                        </div>
                    </div>

                    <div class="listview__scroll scrollbar-inner">
                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/1.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">David Belle</div>
                                <p>Cum sociis natoque penatibus et magnis dis parturient montes</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/2.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Jonathan Morris</div>
                                <p>Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/3.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Fredric Mitchell Jr.</div>
                                <p>Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/4.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Glenn Jecobs</div>
                                <p>Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/5.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Bill Phillips</div>
                                <p>Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/1.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">David Belle</div>
                                <p>Cum sociis natoque penatibus et magnis dis parturient montes</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/2.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Jonathan Morris</div>
                                <p>Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</p>
                            </div>
                        </a>

                        <a href="" class="listview__item">
                            <img src="{{ Asset('/assets/material/demo/img/profile-pics/3.jpg') }}" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Fredric Mitchell Jr.</div>
                                <p>Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</p>
                            </div>
                        </a>
                    </div>

                    <div class="p-1"></div>
                </div>
            </div> -->
        </li>
    </ul>
</header>
