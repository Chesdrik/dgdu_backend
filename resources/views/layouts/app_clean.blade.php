<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- App styles -->
		<link rel="stylesheet" href="{{ Asset('/css/app_clean.css') }}">

		<title>DeporteUnam</title>
	</head>
	<body>
		@yield('content')
	    @yield('after_includes')
    </body>
</html>
