<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- App styles -->
        <link rel="stylesheet" href="{{ asset('/css/app_clean.css?t='.time()) }}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
		<!-- <link rel="stylesheet" href="{{ asset('/css/ticket.css') }}"> -->

        @livewireStyles
		<title>DeporteUnam</title>
	</head>
	<body>
        <main class="main">

			@yield('modals')
			@include('layouts.includes.header')
			@include('layouts.includes.sidebar')

			<section class="content">
				@if(isset($breadcrumb))
					@include('layouts.includes.breadcrumb')
				@endif
				@yield('content')

				@include('layouts.includes.footer')
			</section>
		</main>

		<!-- App functions and actions -->
		<script src="{{ asset('js/app.js?t='.time()) }}"></script>
        @livewireScripts

	    @yield('after_includes')
    </body>
</html>
