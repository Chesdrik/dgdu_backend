/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


import moment from 'moment';
window.moment = moment;

// Material template JS
require('../material/vendors/jquery/jquery.min.js');
// require('../material/vendors/popper.js/popper.min.js');
require('../material/vendors/bootstrap/js/bootstrap.min.js');
require('../material/vendors/jquery-scrollbar/jquery.scrollbar.min.js');
require('../material/vendors/jquery-scrollLock/jquery-scrollLock.min.js');
require('../material/vendors/flot/jquery.flot.js');
require('../material/vendors/flot/jquery.flot.resize.js');
require('../material/vendors/flot.curvedlines/curvedLines.js');
require('../material/vendors/jqvmap/jquery.vmap.min.js');
require('../material/vendors/jqvmap/maps/jquery.vmap.world.js');
require('../material/vendors/easy-pie-chart/jquery.easypiechart.min.js');
require('../material/vendors/salvattore/salvattore.min.js');
require('../material/vendors/sparkline/jquery.sparkline.min.js');
require('../material/vendors/fullcalendar/es.js');
require('../material/vendors/fullcalendar/fullcalendar.min.js');
require('../ckeditor5-build-classic/ckeditor.js');
require('../ckeditor5-build-classic/vendor.js');

// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
//import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

require('../material/js/app.min.js');
require('../js/app_modified.js');
require('tempusdominus-bootstrap-4');


// ChartJS
require('../js/chartjs/Chart.min.js');

// Flatpickr
require('../material/vendors/flatpickr/flatpickr.js');
require('../material/vendors/flatpickr/flatpickr.min.js');

// DataTables
require('../material/vendors/datatables/jquery.dataTables.min.js');
require('../material/vendors/jszip/jszip.min.js');
// require('../material/vendors/datatables-buttons/dataTables.buttons.min.js');
// require('../material/vendors/datatables-buttons/dataTables.buttons.min.js');
// require('../material/vendors/datatables-buttons/buttons.print.min.js');
