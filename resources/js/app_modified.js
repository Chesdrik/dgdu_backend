'use strict';

/*------------------------------------------------
    Page Loader
-------------------------------------------------*/
$(window).on('load', function () {
    setTimeout(function () {
        $('.page-loader').fadeOut();
    }, 500);
});

$(document).ready(function () {
    setTimeout(function () {
        $('.page-loader').fadeOut();
    }, 500);

    $(".data-table").DataTable({
        autoWidth: !1,
        responsive: !0,
        lengthMenu: [
            [30, 50, 100, -1],
            ["30 registros", "50 registros", "100 registros", "Todos"]
        ],
        language: {
            searchPlaceholder: "Buscar en registros..."
        },
        sDom: '<"dataTables__top"lfB>rt<"dataTables__bottom"ip><"clear">',
        buttons: [
            {
                extend:"excelHtml5",
                title:"Export Data"
            },
            {
                extend:"csvHtml5",
                title:"Export Data"
            },
            {
                extend:"print",
                title:"Material Admin"
            }
        ],
        // "sInfo": "Mostrando de _START_ a _END_ de _TOTAL_ registros",
        "oLanguage": {
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            "sInfoEmpty": 'Hay un total de _TOTAL_ resultado<span class="lvllbl"></span>(s).',
            "sEmptyTable": 'No hay <span class="lvllbl"></span>(s) que mostrar',
            "sZeroRecords": 'No hay resultado<span class="lvllbl"></span>(s) que mostrar',
            "sProcessing": 'Espere - cargando <span class="lvllbl"></span> (s) ...',
            "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
        },
    });
});
