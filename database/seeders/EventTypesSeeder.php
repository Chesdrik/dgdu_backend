<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EventType;

class EventTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DGDU event types
        EventType::create(['name' => 'CUC', 'key' => 'CUC', 'team' => false, 'type' => 'dgdu']);
        EventType::create(['name' => 'AFA', 'key' => 'AFA', 'team' => false, 'type' => 'dgdu']);

        // System event types
        // Single session
        EventType::create(['name' => 'Caminatas', 'key' => 'individual_caminatas', 'team' => false, 'type' => 'local']);
        EventType::create(['name' => 'Acondicionamiento físico', 'key' => 'individual_acondicionamiento', 'team' => false, 'type' => 'local']);
        EventType::create(['name' => 'Entrenamiento', 'key' => 'individual_entrenamiento', 'team' => false, 'type' => 'local']);
        EventType::create(['name' => 'Remadora fija', 'key' => 'individual_remadora_fija', 'team' => false, 'type' => 'local']);
        EventType::create(['name' => 'Regata canotaje', 'key' => 'individual_regata_canotaje', 'team' => false, 'type' => 'local']);
        EventType::create(['name' => 'Regata remo', 'key' => 'individual_regata_remo', 'team' => false, 'type' => 'local']);

        // Multiple sessions: Minutos acumulados (individual, pareja, mas)
        EventType::create(['name' => 'Banda', 'key' => 'team_banda', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'A.F.', 'key' => 'team_af', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Bicicleta fija', 'key' => 'team_bicicleta_fija', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Remadora', 'key' => 'team_remadora', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Entrenamiento', 'key' => 'team_entrenamiento', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Carrera', 'key' => 'team_carrera', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Bicicleta', 'key' => 'team_bicicleta', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Caminata', 'key' => 'team_caminata', 'team' => true, 'type' => 'local']);
        EventType::create(['name' => 'Remo', 'key' => 'team_remo', 'team' => true, 'type' => 'local']);
    }
}
