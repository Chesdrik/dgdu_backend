<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        DB::table('users')->insert([
            'user_type_id' => 1,
            'name' => $faker->name,
            'last_name' => $faker->name,
            'second_last_name' => $faker->name,
            'CURP' => 'JAFJ910504MSA',
            'username' => $faker->name,
            'password' => Hash::make('123456'),
            'email' => $faker->name.'@gmail.com',
            'birthdate' => '1992-11-05',
            'gender' => 'male',
            'weight' => 90,
            'height' => 1.90,
            'worker_number' => 'uui273751',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
