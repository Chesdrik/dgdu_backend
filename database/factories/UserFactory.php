<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Carbon\Carbon;

use Illuminate\Support\Facades\Hash;


class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_type_id' => 1,
            'name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'second_last_name' => $this->faker->name,
            'CURP' => 'JAFJ910504MSA',
            'username' => $this->faker->name,
            'password' => Hash::make('123456'),
            'email' => $this->faker->unique()->safeEmail,
            'birthdate' => '1992-11-05',
            'gender' => 'male',
            'weight' => 90,
            'height' => 1.90,
            'worker_number' => 'uui273751',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];
    }
}


