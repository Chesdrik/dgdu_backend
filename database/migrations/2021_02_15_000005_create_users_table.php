<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_type_id');
            $table->string('name', 45)->nullable()->default(null);
            $table->string('last_name', 45)->nullable()->default(null);
            $table->string('second_last_name', 45)->nullable()->default(null);
            $table->string('CURP', 45)->nullable()->default(null);
            $table->string('username', 45)->nullable()->default(null);
            $table->string('password', 145)->nullable()->default(null);
            $table->string('email', 45)->nullable()->default(null);
            $table->date('birthdate')->nullable()->default(null);
            $table->enum('gender', ['female', 'male'])->nullable()->default(null);
            $table->integer('weight')->nullable()->default(null);
            $table->double('height')->nullable()->default(null);
            $table->string('worker_number', 45)->nullable()->default(null);
            $table->string('campus', 45)->nullable()->default(null);
            $table->string('career', 45)->nullable()->default(null);
            $table->string('master', 45)->nullable()->default(null);

            $table->index(["user_type_id"], 'fk_user_user_types_idx');
            $table->nullableTimestamps();


            $table->foreign('user_type_id', 'fk_user_user_types_idx')
                ->references('id')->on('user_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
