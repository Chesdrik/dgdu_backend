<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'stats';

    /**
     * Run the migrations.
     * @table stats
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stat_type_id');
            $table->unsignedBigInteger('effort_user_id');
            $table->string('value', 45)->nullable()->default(null);

            $table->index(["effort_user_id"], 'fk_stats_effort_user1_idx');

            $table->index(["stat_type_id"], 'fk_stats_stat_types1_idx');


            $table->foreign('effort_user_id', 'fk_stats_effort_user1_idx')
                ->references('id')->on('effort_users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('stat_type_id', 'fk_stats_stat_types1_idx')
                ->references('id')->on('stat_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
