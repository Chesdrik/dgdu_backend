<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCategoriesDumbass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->renameColumn('cod_categorie', 'cod_category');
        });

        Schema::table('event_user', function (Blueprint $table) {
            $table->renameColumn('categorie_id', 'category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('categories', function (Blueprint $table) {
            $table->renameColumn('cod_category', 'cod_categorie');
        });

        Schema::table('event_user', function (Blueprint $table) {
            $table->renameColumn('category_id', 'categorie_id');
        });
    }
}
