<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'sliders';

    /**
     * Run the migrations.
     * @table sliders
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sliders')) {
            Schema::disableForeignKeyConstraints();
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id');
                $table->string('title', 145)->nullable();
                $table->string('url', 145)->nullable();
                $table->string('img', 145)->nullable();
                $table->tinyInteger('active')->nullable();
                $table->integer('order')->nullable();
            });
            Schema::enableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
