<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEffortUserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'effort_user';

    /**
     * Run the migrations.
     * @table effort_user
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('effort_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('stat_id');
            $table->timestamp('start')->nullable()->default(null);
            $table->timestamp('end')->nullable()->default(null);

            $table->index(["effort_id"], 'fk_efforts_has_users_efforts1_idx');

            $table->index(["user_id"], 'fk_efforts_has_users_users1_idx');

            $table->index(["stat_id"], 'fk_effort_users_stats1_idx');


            $table->foreign('stat_id', 'fk_effort_users_stats1_idx')
                ->references('id')->on('stats')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('effort_id', 'fk_efforts_has_users_efforts1_idx')
                ->references('id')->on('effort_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('user_id', 'fk_efforts_has_users_users1_idx')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
