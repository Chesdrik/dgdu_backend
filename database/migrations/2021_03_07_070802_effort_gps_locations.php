<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EffortGpsLocations extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'effort_gps_locations';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->tableName)) {
            Schema::disableForeignKeyConstraints();
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->id();
                $table->foreignId('effort_id');
                // $table->decimal('lat', 10, 7);
                // $table->decimal('long', 10, 7);
                // $table->date('datetime');
                $table->mediumText('route');
            });
            Schema::enableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('effort_gps_locations');
    }
}
