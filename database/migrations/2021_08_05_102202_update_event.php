<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('date', 'start');
            $table->dateTime('date')->nullable()->default(null)->change();

            $table->dateTime('end')->nullable()->default(null)->after('date');
            $table->integer('max_users')->default(1)->after('end');
            $table->foreignId('event_type_id')
                ->after('noid')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('event_types')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('start', 'date');
            $table->dateTime('date')->nullable()->default(null)->change();

            $table->dropColumn('end');
            $table->dropColumn('max_users');
            $table->dropForeign(['event_type_id']);
            $table->dropColumn('event_type_id');
        });
    }
}
