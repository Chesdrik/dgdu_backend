<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\SiteController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ResultEventController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\EffortController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\VideoController;



// Home controller
Route::get('/', [SiteController::class, 'home']);
// Route::get('/', [SiteController::class, 'encryption']);

Route::group(['middleware' => 'auth'], function () {
    // Dashboard
    Route::get('/dashboard', [SiteController::class, 'dashboard'])->name('dashboard');

    // Events
    Route::resource('/eventos', EventController::class)->names('events');

    Route::post('/eventos/{id}/importar/resultados', [App\Http\Controllers\ResultEventController::class, 'import'])->name('event.import');
    Route::get('/eventos/{id}/importar/', [App\Http\Controllers\ResultEventController::class, 'index'])->name('event.import_index');

    // Sliders
    Route::resource('/sliders', SliderController::class, [
        'names' => [
            'index' => 'sliders.index',
            'store' => 'sliders.store',
            'update' => 'sliders.update',
            'create' => 'sliders.create',
            'show' => 'sliders.show',
            'destroy' => 'sliders.destroy',
        ],
    ]);

    // Users
    Route::get('/usuarios/administradores', [UserController::class, 'index'])->name('users.index.admins');
    Route::get('/usuarios/usuariosApp', [UserController::class, 'appUsers'])->name('users.index.appusers');
    Route::resource('/usuarios', UserController::class, [
        'names' => [
            'index' => 'users.index',
            'store' => 'users.store',
            'update' => 'users.update',
        ],
        'parameters' => [
            'usuario' => 'users',
            'usuarios' => 'users'
        ]
    ]);
    Route::get('/usuarios/{id}/efforts', [UserController::class, 'efforts'])->name('users.effort');
    Route::get('/usuarios/{id}/efforts', [UserController::class, 'efforts'])->name('users.effort');
    Route::get('esfuerzo/{id}/kml', [EffortController::class, 'kml_download'])->name('kml_download');

    // News
    Route::get('/noticias/rss_fetch', [NewsController::class, 'rss_fetch'])->name('news.rss_fetch');
    Route::resource('/noticias', NewsController::class, [
        'names' => [
            'index' => 'news.index',
            'store' => 'news.store',
            'update' => 'news.update',
            'create' => 'news.create',
            'show' => 'news.show',
            'destroy' => 'news.destroy',
        ],
        'parameters' => [
            'noticia' => 'news',
            'noticias' => 'news'
        ]
    ]);

    Route::resource('/videos', VideoController::class, [
        'names' => [
            'index' => 'video.index',
            'store' => 'video.store',
            'update' => 'video.update',
            'create' => 'video.create',
            'show' => 'video.show',
            'destroy' => 'video.destroy',
        ],
        'parameters' => [
            'video' => 'video',

        ]
    ]);



    // Historial
    // Route::get('/historial', [APIController::class, 'get_historial']);

    Route::resource('/textos',  ContentController::class, [
        'names' => [
            'index' => 'content.index',
            'show' => 'content.show',
            'update' => 'content.update',
            'destroy' => 'content.destroy'
        ]
    ]);
});

Auth::routes();
