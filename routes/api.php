<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\APIController;
use \App\Http\Controllers\API\EventController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/amigos', [APIController::class, 'friend_pending']);

// Login
Route::post('/login', [APIController::class, 'ApiLogin']);
Route::post('/reset_password', [APIController::class, 'resetPassword']);


// Events
Route::post('/events/{user_id}', [APIController::class, 'eventUser']);
Route::post('/event/{user_id}/{event_id}', [APIController::class, 'eventDetail']);
Route::post('/user/event/register', [APIController::class, 'registerUserEvent']); // For DGDU SOAP API

// New events API
Route::post('/user/event/register', [EventController::class, 'register_to_event']);

Route::post('/event', [APIController::class, 'listEvent']);
Route::post('/event/{event_id}', [APIController::class, 'detailEvent']);
Route::get('/categories', [APIController::class, 'getCategories']);

//teams
Route::post('/team/create', [APIController::class, 'create_team']);
Route::post('/team/add_user', [APIController::class, 'add_user_team']);
Route::post('/team/list/user', [APIController::class, 'list_user_team']);
Route::post('/team/add_time/user', [APIController::class, 'add_result']);


//friends
Route::post('/friendship', [APIController::class, 'friends']);
Route::post('/pending/friendship', [APIController::class, 'friends_pending']);
Route::post('/my_pending/friendship', [APIController::class, 'friends_Mypending']);
Route::post('/new/friendship', [APIController::class, 'add_friend']);
Route::post('/change_ststus/friendship', [APIController::class, 'change_status_friend']);
Route::post('/details/friendship', [APIController::class, 'friends_details']);
Route::post('/reactions/friendship', [APIController::class, 'reactions']);

// Sliders
Route::post('/dashboard', [APIController::class, 'dashboard']);
Route::post('/effort/detail', [APIController::class, 'effort_detail']);
Route::post('/effort/register', [APIController::class, 'register_effort']); // Only while we merge dbs
Route::post('/historial/{noCta}', [APIController::class, 'get_historial']);
Route::post('/update_user_data', [APIController::class, 'updateUserData']);

Route::post('/result/{event}/{user}', [APIController::class, 'result_event']);

Route::post('/videos', [APIController::class, 'videos']);
Route::post('/content', [APIController::class, 'content']);

Route::post('/result/{event}/{user}/', [APIController::class, 'result_event']);

// Users
Route::post('/register', [APIController::class, 'register']);
